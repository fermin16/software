package com.example.PampApp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.PampApp.Modelos.ElementoListaAlerta;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;

public class cercanas extends AppCompatActivity implements PermissionsListener {


    //variables para la ubicacion
    private Double latitud;
    private Double longitud;
    private Geocoder geocoder;
    private PermissionsManager permissionsManager;
    private final int ACTIVAR_UBICACION = 0; //Codigo de la subactividad que nos servira para saber si el usuario a activado o no la ubicacion
    //variables lista
    private ListView lista;
    private View mProgressView; //Campo para la animacion de la progress bar
    ArrayList<ElementoListaAlerta> alertas;
    String user;
    //variables consulta base de datos

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Inicializar interfaz
        super.onCreate(savedInstanceState);
        Ajustes.onActivityCreateSetAjustes(this,0);
        setContentView(R.layout.activity_cercanas);
        user = getIntent().getStringExtra("user");
        lista = (ListView) findViewById(R.id.listaCercanas);
        alertas = new ArrayList<>();
        mProgressView = findViewById(R.id.cercanas_progress);
        showProgress(true);
        actualizarLista();
        lista.setAdapter(new ListaAdapterAlert(alertas, this));
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent verAlerta = new Intent(view.getContext(), com.example.PampApp.verAlerta.class);
                verAlerta.putExtra("user", user);
                verAlerta.putExtra("id", alertas.get(position).getId());
                startActivityForResult(verAlerta,1);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 1) {
            actualizarLista();
        }
    }

    public void actualizarLista() {
        ParseQuery<ParseObject> query;
        obtenerUbicacion();
        if (latitud != null && longitud != null) {
            query = ParseQuery.getQuery("Alert");
            geocoder = new Geocoder(getApplicationContext());
            ParseGeoPoint userLocation = new ParseGeoPoint(latitud, longitud);
            query.whereWithinKilometers("localizacion", userLocation, Map.MAX_DISTANCIA);
            query.setLimit(20);
            Toast.makeText(this, getString(R.string.obteniendoAlertas)+"("+Map.MAX_DISTANCIA+" km)", Toast.LENGTH_SHORT).show();
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> query, ParseException e) {
                    final ArrayList<ElementoListaAlerta> auxAlertas = new ArrayList<>();
                    if (e == null) {
                        for (ParseObject obj : query) {
                            String titulo = obj.get("titulo").toString();
                            byte[] foto = (byte[]) obj.get("foto");
                            String id = obj.getObjectId();
                            ParseGeoPoint auxlocalizacion = obj.getParseGeoPoint("localizacion");
                            double aux1 = auxlocalizacion.getLatitude();
                            double aux2 = auxlocalizacion.getLongitude();
                            String direccion;
                            try { //Obtener la direccion, printearla en el cuadro de texto y mostrarla
                                List<Address> address = geocoder.getFromLocation(auxlocalizacion.getLatitude(), auxlocalizacion.getLongitude(), 1);
                                if (!address.isEmpty()) {
                                    direccion = address.get(0).getAddressLine(0);
                                } else {
                                    direccion = getString(R.string.direccionNoEncontrada);
                                }
                            } catch (IOException u) { //En caso de que el usuario no tenga cnoexion a internet:
                                direccion = getString(R.string.direccionNoEncontrada);
                            }
                            ElementoListaAlerta nuevo = new ElementoListaAlerta(foto, titulo, direccion, id);
                            auxAlertas.add(nuevo);
                        }
                        ListaAdapterAlert adpater = new ListaAdapterAlert(auxAlertas, getApplicationContext());
                        lista.setAdapter(adpater);
                        saveList(auxAlertas);
                        showProgress(false);
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_conexion), Toast.LENGTH_SHORT).show();
                        showProgress(false);
                        finish();
                    }
                }
            });
        } else {
            showProgress(false);
            Toast.makeText(this, getString(R.string.errorUbicacion), LENGTH_LONG).show();
        }
    }

    private void saveList(List<ElementoListaAlerta> result) {
        alertas.addAll(result);
    }


    /* Metodo que chequea los permisos de ubicacion, tras esto chequea que los servicios de red y
     de ubicacion esten activos y en caso de estarlo intenta obtener la ultima ubicacion del usuario.
     */
    @SuppressLint("MissingPermission")
    private void obtenerUbicacion() {
        //Comprobar si se han concedido los permisos de ubicacion:
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            if (checkLocationServices()) {//Comprobar si estan activados los servicios de red y ubicacion.
                Location loc = getLastKnownLocation(); //Obtener la ubicacion
                if (loc != null) {
                    latitud = loc.getLatitude();
                    longitud = loc.getLongitude();
                } else {
                    Toast.makeText(this, getString(R.string.errorUbicacion), Toast.LENGTH_SHORT).show();
                }
            }
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }

    }

    /* Metodo que permite chequear los servicios de acceso para ver si estan activados o no. Devuelve
     *  falso si estan desactivados y verdadero en caso contrario.*/
    public boolean checkLocationServices() {
        LocationManager lm = (LocationManager) this.getSystemService(this.LOCATION_SERVICE);
        boolean gps = false;
        boolean network = false;

        try {
            gps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }
        if (!gps) {
            //Notificar al usuario que tiene desactivado el gps.
            new AlertDialog.Builder(this,Temas.THEME_DIALOG)
                    .setMessage(R.string.gps_desactivado)
                    .setPositiveButton(R.string.activar_gps, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), ACTIVAR_UBICACION);
                        }
                    }).setNegativeButton(R.string.cancelar_gps, null)
                    .show();
            return false;
        } else if (!network) {
            //Notificar al usuario que tiene desactivado el gps.
            new AlertDialog.Builder(this,Temas.THEME_DIALOG)
                    .setMessage(R.string.net_desactivada)
                    .setPositiveButton(R.string.activar_gps, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), ACTIVAR_UBICACION);
                        }
                    }).setNegativeButton(R.string.cancelar_gps, null)
                    .show();
            return false;
        }
        return true;
    }

    /*Metodo que obtiene la ultima ubicacion del usuario, para ello comprueba cada uno de los proveedores
     * ubicacion y se queda con aquel que sea mas preciso. Por ultimo devulve la ubicacion del usuario */
    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true); //Obtener solo los proveedores activos
        Location bestLocation = null; //Variable que almacena la mejor ubicacion
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, getString(R.string.user_location_permission_explanation), LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            obtenerUbicacion();
        } else {
            Toast.makeText(this, getString(R.string.user_location_permission_not_granted), LENGTH_LONG).show();
        }
    }

    /**
     * Muestra la animacion de la UI y esconde la lista de alertas cercanas:
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            lista.setVisibility(show ? View.GONE : View.VISIBLE);
            lista.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    lista.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            lista.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
