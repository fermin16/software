package com.example.PampApp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PampApp.Modelos.Preferencias;
import com.example.PampApp.Modelos.User;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.List;

/** Clase Cuenta: La clase cuenta muestra el perfil del usuario de la aplicación, es decir, su nombre, apllidos,
 * puntuación... además permite a el usuario poder cambiar sus preferencias como su correo o su contraseña.**/
public class miCuenta extends AppCompatActivity {

    //Campos que muestran la info del usuario:
    private TextView nombre;
    private TextView ap1;
    private TextView ap2;
    private TextView dni;
    private TextView email;

    //Variable modificar activity_ajustes:
    private CardView cardmodificar;
    private Button cambiarCorreo;
    private Button cambiarPassword;

    //Campos para modificar correo y contraseña del usuario:
    private CardView modificarAjustes;
    private TextView tituloCambiar;
    private TextView cambiarTvAntiguo;
    private TextView cambiarTvNuevo;
    private TextView cambiarTvNuevo2;
    private EditText cambiarETAntiguo;
    private EditText cambiarETNuevo;
    private EditText cambiarETNuevo2;
    private Button guardar;
    private Button cancelar;

    private View mProgressView; //Campo para la animacion de la progress bar
    private View miCuenta; //Campo que contiene el relativeLayout

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Ajustes.onActivityCreateSetAjustes(this,0);
        setContentView(R.layout.mi_cuenta);

        mProgressView = findViewById(R.id.progressBar); //Inicializar el campo para la animacion.
        miCuenta = findViewById(R.id.miCuentaLayout); //Inicializar el campo del relative layout
        showProgress(true);

        //Variable para mostrar los datos del usuario:
        nombre = findViewById(R.id.nombreUsuario);
        ap1 = findViewById(R.id.ap1Usuario);
        ap2 = findViewById(R.id.ap2Usuario);
        dni = findViewById(R.id.DNIUsuario);
        email = findViewById(R.id.emailUsuario);
        final TextView numVotos = findViewById(R.id.numVotos);
        final TextView puntuacion = findViewById(R.id.puntuacionTotal);
        final RatingBar rb = findViewById(R.id.ratingBar);

        //Variables para modificar los activity_ajustes del usuario:
        cardmodificar = findViewById(R.id.CardAjustes);
        cardmodificar.setCardBackgroundColor(Color.DKGRAY);
        cambiarCorreo = findViewById(R.id.cambiarCorreo);
        cambiarPassword = findViewById(R.id.cambiarPassword);

        modificarAjustes = findViewById(R.id.modificarAjustes);
        tituloCambiar = findViewById(R.id.cambiarTv);
        cambiarTvAntiguo = findViewById(R.id.antiguoTv);
        cambiarTvNuevo = findViewById(R.id.nuevoTv);
        cambiarTvNuevo2 = findViewById(R.id.nuevoTv2);
        cambiarETAntiguo = findViewById(R.id.antiguoET);
        cambiarETNuevo = findViewById(R.id.nuevoET);
        cambiarETNuevo2 = findViewById(R.id.nuevoET2);
        guardar = findViewById(R.id.botonGuardar);
        cancelar = findViewById(R.id.botonCancelar);

        //Establecer el comportamiento del boton cambiar correo
        cambiarCorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardmodificar.setVisibility(View.INVISIBLE);

                //Colocar el Texto a los TextViews
                tituloCambiar.setText(R.string.modificarCorreo);
                cambiarTvAntiguo.setText(R.string.passwordActual);
                cambiarTvNuevo.setText(R.string.correoNuevo);
                cambiarTvNuevo2.setText(R.string.correoNuevo2);

                //Establecer el tipo de los EditText:
                cambiarETAntiguo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                cambiarETNuevo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                cambiarETNuevo2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

                //Establecer longitud maxima de los EditText:
                InputFilter[] maxLength_correo = new InputFilter[1];
                InputFilter[] maxLength_password = new InputFilter[1];
                maxLength_correo[0] = new InputFilter.LengthFilter(v.getContext().getResources().getInteger(R.integer.maxEmail));
                maxLength_password[0] = new InputFilter.LengthFilter(v.getContext().getResources().getInteger(R.integer.maxPassword));
                cambiarETAntiguo.setFilters(maxLength_password);
                cambiarETNuevo.setFilters(maxLength_correo);
                cambiarETNuevo2.setFilters(maxLength_correo);

                //Resetear los campos de error:
                cambiarETAntiguo.setError(null);
                cambiarETNuevo.setError(null);
                cambiarETNuevo2.setError(null);

                modificarAjustes.setVisibility(View.VISIBLE);

            }
        });

        //Establecer el comportamiento del boton cambiar contraseña:
        cambiarPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardmodificar.setVisibility(View.INVISIBLE);

                //Colocar el Texto a los TextViews
                tituloCambiar.setText(R.string.modificarPassword);
                cambiarTvAntiguo.setText(R.string.passwordAntigua);
                cambiarTvNuevo.setText(R.string.passwordNueva);
                cambiarTvNuevo2.setText(R.string.passwordNueva2);

                //Establecer el tipo de los EditText:
                cambiarETAntiguo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                cambiarETNuevo.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                cambiarETNuevo2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

                //Establecer longitud maxima de los EditText:
                InputFilter[] maxLength = new InputFilter[1];
                maxLength[0] = new InputFilter.LengthFilter(v.getContext().getResources().getInteger(R.integer.maxPassword));
                cambiarETAntiguo.setFilters(maxLength);
                cambiarETNuevo.setFilters(maxLength);
                cambiarETNuevo2.setFilters(maxLength);

                //Resetear los campos de error:
                cambiarETAntiguo.setError(null);
                cambiarETNuevo.setError(null);
                cambiarETNuevo2.setError(null);

                modificarAjustes.setVisibility(View.VISIBLE);
            }
        });

        //Establcer el comportamiento del boton cancelar:
        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(v.getContext(),Temas.THEME_DIALOG)
                        .setTitle(R.string.tituloAlertDialog)
                        .setMessage(R.string.descripcionAlertDialog)

                        // Specifying a listener allows you to take an action before dismissing the dialog.
                        // The dialog is automatically dismissed when a dialog button is clicked.
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                resetearCard();
                            }
                        })

                        // A null listener allows the button to dismiss the dialog and take no further action.
                        .setNegativeButton(android.R.string.no, null)
                        .setIcon(android.R.drawable.btn_dialog)
                        .show();
            }
        });

        //Establecer el comportamiento del boton guardar:
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String Antiguo = cambiarETAntiguo.getText().toString();
                final String Nuevo = cambiarETNuevo.getText().toString();
                final String Confirma = cambiarETNuevo2.getText().toString();
                View focusView = new View(v.getContext());

                //Comprobar si se esta modificando el email o la constraseña:
                if(tituloCambiar.getText().toString().compareTo(getString(R.string.modificarCorreo)) == 0) { //En caso de que sea email:
                    if (SignInActivity.checkPassword(Antiguo, cambiarETAntiguo, v.getContext(), focusView)){ //Comprobar la contraseña actual
                        if (Antiguo.compareTo(Preferencias.getPasswordUsuario(v.getContext())) == 0) { //Comprobar que la contraseña actual coincide
                            if (!SignInActivity.checkEmails(Nuevo, Confirma, cambiarETNuevo, cambiarETNuevo2, v.getContext(), focusView))
                                focusView.requestFocus();
                            else {
                                if (Nuevo.compareTo(Preferencias.getEmailUsuario(v.getContext())) != 0) { //Comprobar que el email nuevo no coincida con el antiguo
                                    new AlertDialog.Builder(v.getContext(),Temas.THEME_DIALOG)
                                            .setTitle(R.string.tituloAlertEmail)
                                            .setMessage(R.string.descripcionAlertEmail)

                                            // Specifying a listener allows you to take an action before dismissing the dialog.
                                            // The dialog is automatically dismissed when a dialog button is clicked.
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    showProgress(true); //Mostrar la animacion:
                                                    Toast.makeText(getApplicationContext(),getString(R.string.actualizandoEmail),Toast.LENGTH_SHORT).show();
                                                    //Buscar el objeto en la base de datos y actualizar el email:
                                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
                                                    query.whereEqualTo(User.EMAIL,Preferencias.getEmailUsuario(getApplicationContext()));

                                                    query.getFirstInBackground(new GetCallback<ParseObject>() {
                                                        public void done(ParseObject object, ParseException e) {
                                                            if (e == null) {
                                                                if (object != null) { //Si hay coincidencia con el usuario que desea cambiar su email
                                                                    ParseQuery<ParseObject> query2 = ParseQuery.getQuery("User");
                                                                    query2.whereEqualTo(User.EMAIL, Nuevo);
                                                                    try {
                                                                        if (query2.count() != 1) {//Si hay no coincidencia de email:
                                                                            object.put(User.EMAIL, Nuevo);
                                                                            object.saveInBackground();
                                                                            Toast.makeText(getApplicationContext(), getString(R.string.emailActualizado), Toast.LENGTH_SHORT).show();
                                                                            Preferencias.guardarEmailUsuario(getApplicationContext(),Nuevo);
                                                                            resetearCard();

                                                                            //Iniciar el activity de Login y borrar la pila de actividades anteriores.
                                                                            pantallaPrincipal.cerrarSesion(miCuenta.this);
                                                                        } else {
                                                                            cambiarETNuevo.setError(getString(R.string.email_ya_registrado));
                                                                        }
                                                                    } catch (ParseException e1) {
                                                                        Toast.makeText(getApplicationContext(), getString(R.string.error_conexion), Toast.LENGTH_SHORT).show();
                                                                    }
                                                                } else {
                                                                    Toast.makeText(getApplicationContext(),getString(R.string.usuarioNoEncontrado),Toast.LENGTH_SHORT).show();
                                                                    pantallaPrincipal.cerrarSesion(miCuenta.this);
                                                                }
                                                            }
                                                            showProgress(false);
                                                        }
                                                    });
                                                }
                                            })

                                            // A null listener allows the button to dismiss the dialog and take no further action.
                                            .setNegativeButton(android.R.string.no, null)
                                            .setIcon(android.R.drawable.ic_dialog_info)
                                            .show();
                                }
                                else{
                                    cambiarETNuevo.setError(getString(R.string.emailNuevoError));
                                }
                            }
                        }
                        else{
                            cambiarETAntiguo.setError(getString(R.string.passwdActualError));
                        }
                    }
                }
                else{//En caso de que sea contraseña:
                    if (SignInActivity.checkPassword(Antiguo, cambiarETAntiguo, v.getContext(), focusView)){ //Comprobar el campo de password antiguo
                        if (Antiguo.compareTo(Preferencias.getPasswordUsuario(v.getContext())) == 0) { //Comprobar que la password antigua coincide
                            if (!SignInActivity.checkpasswords(Nuevo, Confirma, cambiarETNuevo, cambiarETNuevo2, v.getContext(), focusView))
                                focusView.requestFocus();
                            else {
                                if (Nuevo.compareTo(Antiguo) != 0){ //Comprobar que la password nueva no coincida con la antigua
                                    new AlertDialog.Builder(v.getContext(),Temas.THEME_DIALOG)
                                            .setTitle(R.string.tituloAlertPassword)
                                            .setMessage(R.string.descripcionAlertPassword)

                                            // Specifying a listener allows you to take an action before dismissing the dialog.
                                            // The dialog is automatically dismissed when a dialog button is clicked.
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    Toast.makeText(getApplicationContext(),getString(R.string.actualizandoPassword),Toast.LENGTH_SHORT).show();
                                                    showProgress(true);
                                                    //Buscar el objeto en la base de datos y actualizar la password:
                                                    ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
                                                    query.whereEqualTo(User.EMAIL, Preferencias.getEmailUsuario(getApplicationContext()));

                                                    query.getFirstInBackground(new GetCallback<ParseObject>() {
                                                        public void done(ParseObject object, ParseException e) {
                                                            if (e == null) {
                                                                if (object != null) { //Si hay coincidencia con el usuario que desea cambiar su email
                                                                    object.put(User.PASSWORD, Nuevo);
                                                                    Toast.makeText(getApplicationContext(), getString(R.string.passwordActualizada), Toast.LENGTH_SHORT).show();
                                                                    object.saveInBackground();
                                                                    resetearCard();

                                                                    //Iniciar el activity de Login y borrar la pila de actividades anteriores.
                                                                    pantallaPrincipal.cerrarSesion(miCuenta.this);
                                                                } else {
                                                                    Toast.makeText(getApplicationContext(),getString(R.string.usuarioNoEncontrado),Toast.LENGTH_SHORT).show();
                                                                    pantallaPrincipal.cerrarSesion(miCuenta.this);
                                                                }
                                                            }
                                                            showProgress(false);
                                                        }
                                                    });
                                                }
                                            })

                                            // A null listener allows the button to dismiss the dialog and take no further action.
                                            .setNegativeButton(android.R.string.no, null)
                                            .setIcon(android.R.drawable.ic_dialog_info)
                                            .show();
                                }
                                else{
                                    cambiarETNuevo.setError(getString(R.string.passwordNuevaError));
                                }
                            }
                        }
                        else{
                            cambiarETAntiguo.setError(getString(R.string.passwordAntiguoError));
                        }
                    }
                }
            }
        });

        nombre.setText(getString(R.string.nombre)+" "+ Preferencias.getNombreUsuario(this));
        ap1.setText(getString(R.string.ap1)+" "+ Preferencias.getAp1Usuario(this));
        ap2.setText(getString(R.string.ap2)+" "+ Preferencias.getAp2Usuario(this));
        dni.setText("DNI: "+ Preferencias.getDNIUsuario(this));
        email.setText("Email: "+ Preferencias.getEmailUsuario(this));

        //Campos por defecto:
        puntuacion.setText(getString(R.string.puntuacionTotal)+" "+0);
        numVotos.setText(getString(R.string.numVotos)+" "+0);
        rb.setRating(0f);

        //Puntuación debe ser obtenida del servidor cada vez que el usuario entre en su cuenta:
        final ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
        Toast.makeText(getApplicationContext(),getString(R.string.obteniendoPuntuacion),Toast.LENGTH_SHORT).show();
        query.whereEqualTo(User.EMAIL,Preferencias.getEmailUsuario(this));
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    if(!objects.isEmpty()) {
                        if(objects.size() > 0) {
                            ParseObject obj = objects.get(0);
                            if((int) obj.get(User.VOTOS) > 0){
                                puntuacion.setText(getString(R.string.puntuacionTotal)+" "+((int)obj.get(User.PUNT)));
                                numVotos.setText(getString(R.string.numVotos)+" "+((int)obj.get(User.VOTOS)));
                                rb.setRating(((int)obj.get(User.PUNT))/((int)obj.get(User.VOTOS)));
                            }
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(),getString(R.string.errorPuntuacion),Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),getString(R.string.errorConexionServ),Toast.LENGTH_SHORT).show();
                }
                showProgress(false);
            }
        });
    }

    /*Metodo para resetear el card de activity_ajustes: */
    private void resetearCard(){
        modificarAjustes.setVisibility(View.INVISIBLE);
        cambiarETAntiguo.setText(null);
        cambiarETNuevo.setText(null);
        cambiarETNuevo2.setText(null);
        cambiarETAntiguo.setError(null);
        cambiarETNuevo.setError(null);
        cambiarETNuevo2.setError(null);
    }

    //Metodos para construir un menu y poder mostrar un boton en la app bar:
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mi_cuenta_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Handler para el boton del menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.botonAjustes) {
            if(modificarAjustes.getVisibility() == View.INVISIBLE) {
                if (cardmodificar.getVisibility() == View.INVISIBLE)
                    cardmodificar.setVisibility(View.VISIBLE);
                else
                    cardmodificar.setVisibility(View.INVISIBLE);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    //Controlar el funcionamiento de la tecla atras:
    @Override
    public void onBackPressed() {
        if (cardmodificar.getVisibility() == View.VISIBLE) { //Si el card para elegir modificar el correo o contraseña esta visible
            cardmodificar.setVisibility(View.INVISIBLE);
        } else if (modificarAjustes.getVisibility() == View.VISIBLE){ //Si el card para modificar el corre oo contraseña esta visible
            cancelar.performClick();
        }
        else{ //Si no terminar la actividad.
            finish();
        }
    }

    /**
     * Este metodo se encarga de mostrar la animacion del spinner cuando un usuario accede a su cuenta.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            miCuenta.setVisibility(show ? View.GONE : View.VISIBLE);
            miCuenta.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    miCuenta.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            miCuenta.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
