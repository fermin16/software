package com.example.PampApp.Modelos;

import com.parse.ParseClassName;
import com.parse.ParseObject;

import java.io.Serializable;

@ParseClassName("Solucion")
public class Solucion extends ParseObject implements Serializable  {

    public Solucion() {}

    public String getUser() {
        return getString("user");
    }

    public void setUser(String user) {
        put("user",user);
    }

    public String getSolucion() {
        return getString("solucion");
    }

    public void setSolucion(String solucion) {
        put("solucion",solucion);
    }
}
