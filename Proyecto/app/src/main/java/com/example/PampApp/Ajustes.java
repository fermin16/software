package com.example.PampApp;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.PampApp.Modelos.Preferencias;

import java.util.Locale;

/**
 * Actividad de Ajustes del sistema: Esta acividad contiene la lógica de los ajustes de la aplicación
 * Las funcionalidades que contiene esta actividad son:
 * -Cambiar el tema de fondo de la APP.
 * -Cambiar el color de la barra de estado.
 * -Ca,biar el idioma de la apliacación.
 */
public class Ajustes extends AppCompatActivity implements View.OnClickListener, Temas, Idiomas {

    //Variables para elementos estéticos de la aplicación:
    private RadioButton temaClaro;
    private RadioButton temaOscuro;
    private RadioButton azul;
    private RadioButton verde;
    private RadioButton rojo;
    private Button guardar;
    private Button cancelar;
    private CheckBox idioma_es;
    private CheckBox idioma_en;

    //Variables para las funcionalidades de la palicación:
    /** idiomaSeleccionado y temaSeleccionado serán variables estáticas lo que permitirá  determinar desde
    la pantalla principal si han habido cambios en los ajustes **/
    public static String idiomaSeleccionado;
    public static int temaSeleccionado;
    private String idiomaActual;
    private  int temaActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onActivityCreateSetAjustes(this,0);
        setContentView(R.layout.activity_ajustes);
        temaClaro = findViewById(R.id.temaClaro);
        temaOscuro = findViewById(R.id.temaOscuro);
        azul = findViewById(R.id.colorAzul);
        rojo = findViewById(R.id.colorRojo);
        verde = findViewById(R.id.colorVerde);

        //Guardar el tema e idioma actual:
        temaActual = Preferencias.getTema(this);
        idiomaActual = Preferencias.getIdioma(this);

        idioma_es = findViewById(R.id.idiomaES); //Inicializar el boton de español
        idioma_en = findViewById(R.id.idiomaEN); //Inicializar el boton de inglés
        idioma_en.setOnClickListener(this);
        idioma_es.setOnClickListener(this);

        if(temaSeleccionado == 0) { //Si no hay un tema seleccionado, comprobar el tema actual para poder determinar que botones marcar
            if (temaActual == THEME_DEFAULT) {
                temaClaro.setChecked(true);
                verde.setChecked(true);
            }

            else if(temaActual == THEME_DARK){
                temaOscuro.setChecked(true);
                verde.setChecked(true);
            }

            else if(temaActual == THEME_BLUE_LIGHT){
                temaClaro.setChecked(true);
                azul.setChecked(true);

            }else if(temaActual == THEME_BLUE_DARK) {
                temaOscuro.setChecked(true);
                azul.setChecked(true);
            }
             else if(temaActual == THEME_RED_LIGHT) {
                temaClaro.setChecked(true);
                rojo.setChecked(true);
            }
            else if(temaActual == THEME_RED_DARK) {
                temaOscuro.setChecked(true);
                rojo.setChecked(true);
            }
            temaSeleccionado = temaActual;
        }
        else{ //Si hay un tema seleccionado marcar los botones que correspondan con dicho tema.
            if (temaSeleccionado == THEME_DEFAULT) {
                temaClaro.setChecked(true);
                verde.setChecked(true);
            }

            else if(temaSeleccionado == THEME_DARK){
                temaOscuro.setChecked(true);
                verde.setChecked(true);
            }

            else if(temaSeleccionado == THEME_BLUE_LIGHT){
                temaClaro.setChecked(true);
                azul.setChecked(true);

            }else if(temaSeleccionado == THEME_BLUE_DARK) {
                temaOscuro.setChecked(true);
                azul.setChecked(true);
            }
            else if(temaSeleccionado == THEME_RED_LIGHT) {
                temaClaro.setChecked(true);
                rojo.setChecked(true);
            }
            else if(temaSeleccionado == THEME_RED_DARK) {
                temaOscuro.setChecked(true);
                rojo.setChecked(true);
            }
        }

        //Comprobar los botones de idioma:
        if(idiomaSeleccionado == null){ //Si no se ha seleccionado un idioma.
            if(idiomaActual.compareTo(IDIOMA_DEFECTO) == 0){
                idioma_es.setChecked(true);
            }
            else{
                idioma_en.setChecked(true);
            }
            idiomaSeleccionado = idiomaActual;
        }
        else{ //Si se ha seleccionado un idioma.
            if(idiomaSeleccionado.compareTo(IDIOMA_DEFECTO) == 0)
                idioma_es.setChecked(true);
            else
                idioma_en.setChecked(true);
        }

        //Cambiar el color del texto de los botones de selección del color de fondo y de idioma en función del color de fondo de la app:
        if(temaClaro.isChecked()){
            temaClaro.setTextColor(Color.BLACK);
            temaOscuro.setTextColor(Color.BLACK);
            idioma_es.setTextColor(Color.BLACK);
            idioma_en.setTextColor(Color.BLACK);
        }
        else{
            temaClaro.setTextColor(Color.WHITE);
            temaOscuro.setTextColor(Color.WHITE);
            idioma_es.setTextColor(Color.WHITE);
            idioma_en.setTextColor(Color.WHITE);
        }

        temaClaro.setOnClickListener(this);
        temaOscuro.setOnClickListener(this);
        azul.setOnClickListener(this);
        rojo.setOnClickListener(this);
        verde.setOnClickListener(this);

        guardar = findViewById(R.id.guardarAjustes);
        cancelar = findViewById(R.id.cancelarAjustes);

        guardar.setOnClickListener(this);

        cancelar.setOnClickListener(this);

       /*Comprobar el idioma actual y el seleccionado asi como el tema actual y el seleccionado
        * para saber si hay modificaciones en los ajustes, en caso de haberlas se activa el boton
        * guardar*/
        if(temaActual != temaSeleccionado || idiomaActual.compareTo(idiomaSeleccionado) != 0){
                guardar.setEnabled(true);
        }
        else{
                guardar.setEnabled(false);
        }
    }

    /* Metodo que controla los clicks sobre los elementos de los ajustes:  para determinar que elemento
    * se ha pulsado en los ajustes, comprobar el id del objeto pulsado.*/
    @Override
    public void onClick(View v) {
        int idBoton = v.getId();
        Intent intent = new Intent(this,Ajustes.class);

        if (idBoton == R.id.temaOscuro) {
            if (temaClaro.isChecked()) {
                temaClaro.setChecked(false);
                temaOscuro.setChecked(true);
            }

            if(verde.isChecked()){
                cambiaAjustes(this, THEME_DARK, idiomaSeleccionado, intent);
            }
            else if(rojo.isChecked()){
                cambiaAjustes(this,THEME_RED_DARK,idiomaSeleccionado,intent);
            }
            else if(azul.isChecked()){
                cambiaAjustes(this, Temas.THEME_BLUE_DARK,idiomaSeleccionado,intent);
            }
        }
        else if(idBoton == R.id.temaClaro) {
            if (temaOscuro.isChecked()) {
                temaClaro.setChecked(true);
                temaOscuro.setChecked(false);
            }
            if (verde.isChecked()) {
                cambiaAjustes(this, THEME_DEFAULT, idiomaSeleccionado,intent);
            } else if (rojo.isChecked()) {
                cambiaAjustes(this, THEME_RED_LIGHT, idiomaSeleccionado, intent);
            } else if (azul.isChecked()) {
                cambiaAjustes(this,THEME_BLUE_LIGHT, idiomaSeleccionado, intent);
            }
        }
        else if(idBoton == R.id.colorVerde){
            if(rojo.isChecked())
                rojo.setChecked(false);
            else
                azul.setChecked(false);
            verde.setChecked(true);
            if(temaClaro.isChecked()){
                cambiaAjustes(this,THEME_DEFAULT,idiomaSeleccionado,intent);
            }
            else {
                cambiaAjustes(this, THEME_DARK,idiomaSeleccionado,intent);
            }
        }
        else if(idBoton == R.id.colorAzul){
            if(rojo.isChecked())
                rojo.setChecked(false);
            else
                verde.setChecked(false);
            azul.setChecked(true);
            if(temaClaro.isChecked()){
                cambiaAjustes(this, THEME_BLUE_LIGHT, idiomaSeleccionado,intent);
            }
            else {
                cambiaAjustes(this, THEME_BLUE_DARK,idiomaSeleccionado,intent);
            }
        }
        else if(idBoton == R.id.colorRojo){
            if(azul.isChecked())
                azul.setChecked(false);
            else
                verde.setChecked(false);
            rojo.setChecked(true);
            if(temaClaro.isChecked()){
                cambiaAjustes(this, THEME_RED_LIGHT,idiomaSeleccionado,intent);
            }
            else {
                cambiaAjustes(this, THEME_RED_DARK,idiomaSeleccionado,intent);
            }
        }
        else if(idBoton == R.id.idiomaES){
            idioma_es.setChecked(true);
            idioma_en.setChecked(false);
            cambioIdioma(IDIOMA_DEFECTO,this);
            cambiaAjustes(this,temaSeleccionado,IDIOMA_DEFECTO,intent);
        }
        else if(idBoton == R.id.idiomaEN){
            idioma_es.setChecked(false);
            idioma_en.setChecked(true);
            cambioIdioma(IDIOMA_INGLES,this);
            cambiaAjustes(this,temaSeleccionado,IDIOMA_INGLES,intent);
        }
        else if(idBoton == R.id.guardarAjustes){
            Toast.makeText(this,getString(R.string.ajustesActualizados),Toast.LENGTH_SHORT).show();
            finish();
        }
        else{
           cierraAjustes();
        }
    }

    /* Controlar el comportamiento al pulsar la tecla a tras*/
    @Override
    public void onBackPressed() {
        cierraAjustes();
    }

    /*Metodo que se encarga de descartar los ajustes no guardados*/
    public void cierraAjustes(){
        if(temaSeleccionado != temaActual) {
            temaSeleccionado =temaActual;
        }
        if(idiomaSeleccionado.compareTo(idiomaActual) != 0){
            idiomaSeleccionado = idiomaActual;
        }
        finish();
    }

    /* Función que permite cambiar de idioma a la aplicación*/
    public static void cambioIdioma(String idioma, Activity activity){
        Locale myLocale = new Locale(idioma); //Crear un objeto local
        Resources res = activity.getResources(); //Obtener los recursos (res) de la activity)
        DisplayMetrics metrics = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale; //Cambiar la configuracion con el objeto local que contiene el nuevo idioma
        res.updateConfiguration(conf, metrics); //Actualizar la configuración de la apliccación
    }

    /**
     * Metodo que permite cambiar los ajustes de la aplicacion, aplicando los ajustes seleccionados por el usuario.
     * Para ello es necesario terminar la actividad y volver a lanzarla.
     */
    public static void cambiaAjustes(Activity activity, int tema, String idioma, Intent intent) {
        temaSeleccionado = tema;
        idiomaSeleccionado = idioma;
        activity.finish();
        activity.overridePendingTransition(0,0); //Evitamos hacer la animacion de start activity al cambiar ajustes.
        activity.startActivity(intent);
    }

    /** Metodo que permite establecer el tema y el idioma de la aplicacion */
    //Type indica si el tema tiene (0) o no (1) action bar
    public static void onActivityCreateSetAjustes(Activity activity,int type) {
        if(temaSeleccionado == 0){ //Por defecto si no hay tema seleccionado, establecer el tema default
           if(type == 0)
               activity.setTheme(THEME_DEFAULT);
           else
               activity.setTheme(THEME_DEFAULT_NBAR);
        }
        else {
            if(type == 1) {
                activity.setTheme(temaSeleccionado);
                if(temaSeleccionado == THEME_DEFAULT)
                    activity.setTheme(THEME_DEFAULT_NBAR);

                else if(temaSeleccionado == THEME_DARK)
                    activity.setTheme(THEME_DARK_NBAR);

                else if(temaSeleccionado == THEME_BLUE_LIGHT)
                activity.setTheme(THEME_BLUE_LIGHT_NBAR);

                else if(temaSeleccionado == THEME_BLUE_DARK)
                    activity.setTheme(THEME_BLUE_DARK_NBAR);

                else if(temaSeleccionado == THEME_RED_LIGHT)
                    activity.setTheme(THEME_RED_LIGHT_NBAR);

                else if(temaSeleccionado == THEME_RED_DARK)
                    activity.setTheme(THEME_RED_DARK_NBAR);
            }
            else
                activity.setTheme(temaSeleccionado);
        }
        if(idiomaSeleccionado == null){ //Por defecto siempre e pondrá el idioma español
            cambioIdioma(IDIOMA_DEFECTO,activity);
        }
        else
            cambioIdioma(idiomaSeleccionado,activity);
    }
}
