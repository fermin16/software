package com.example.PampApp.Modelos;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.PampApp.Idiomas;
import com.example.PampApp.Temas;

import static android.content.Context.MODE_PRIVATE;

/*Clase preferencias que contiene los metodos para alamcenmar y recuperar datos de una base de datos local
* de la aplicación.*/
public class Preferencias{
    //Constantes para almacenar el inicio de sesion automatico.
    private static final String ID_PREFERENCIAS = "idPreferenciasPampApp";
    private static final String ESTADO_BOTON = "estadoBoton";
    private static final String CORREO_USUARIO = "correoUsuario";
    private static final String NOMBRE_USUARIO = "nombreUsuario";
    private static final String AP1_USUARIO = "ap1Usuario";
    private static final String AP2_USUARIO = "ap2Usuario";
    private static final String DNI_USUARIO = "dniUsuario";
    private static final String PASSWORD_USUARIO = "passwordUsuario";
    private static final String TEMA = "temaFondo";
    private static final  String IDIOMA = "idiomaUsuario";

    //Metodos para inicio de sesion automatico:
    /**Necesitamos un contexto con el que almacenar los datos**/
    public static void guardaEstadoBoton(Context contexto,boolean estadoBoton){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        preferencias.edit().putBoolean(ESTADO_BOTON,estadoBoton).apply();//Guardar el estado del boton.
    }

    public static boolean getEstadoBoton(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        return preferencias.getBoolean(ESTADO_BOTON,false);//Recuperar estado del boton (por defecto falso por si nunca se ha guardado el estado)
    }

    //Metodos para almacenar los datos de usuario.
    public static void guardarEmailUsuario(Context contexto, String emailUsuario){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        preferencias.edit().putString(CORREO_USUARIO,emailUsuario).apply();//Guardar el estado del boton.
    }

    public static String getEmailUsuario(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        return preferencias.getString(CORREO_USUARIO,"");//Recuperar el correo del usuario (por defecto "" por si nunca se ha guardado)
    }

    public static void guardarNombreUsuario(Context contexto, String nombreUsuario){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        preferencias.edit().putString(NOMBRE_USUARIO,nombreUsuario).apply();//Guardar el estado del boton.
    }

    public static String getNombreUsuario(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        return preferencias.getString(NOMBRE_USUARIO,"");//Recuperar el correo del usuario (por defecto "" por si nunca se ha guardado)
    }

    public static void guardarAp1Usuario(Context contexto, String apellido1){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        preferencias.edit().putString(AP1_USUARIO,apellido1).apply();//Guardar el estado del boton.
    }

    public static String getAp1Usuario(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        return preferencias.getString(AP1_USUARIO,"");//Recuperar el correo del usuario (por defecto "" por si nunca se ha guardado)
    }

    public static void guardarAp2Usuario(Context contexto, String apellido2){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        preferencias.edit().putString(AP2_USUARIO,apellido2).apply();//Guardar el estado del boton.
    }

    public static String getAp2Usuario(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        return preferencias.getString(AP2_USUARIO,"");//Recuperar el correo del usuario (por defecto "" por si nunca se ha guardado)
    }

    public static void guardarDNIUsuario(Context contexto, String dni){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        preferencias.edit().putString(DNI_USUARIO,dni).apply();//Guardar el estado del boton.
    }

    public static String getDNIUsuario(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        return preferencias.getString(DNI_USUARIO,"DNI NO ENCONTRADO");//Recuperar el correo del usuario (por defecto "" por si nunca se ha guardado)
    }

    public static void guardarPasswordUsuario(Context contexto, String password){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        preferencias.edit().putString(PASSWORD_USUARIO,password).apply();//Guardar el estado del boton.
    }

    public static String getPasswordUsuario(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);//Generar un fichero interno para almacenar preferencias
        return preferencias.getString(PASSWORD_USUARIO,"");//Recuperar el correo del usuario (por defecto "" por si nunca se ha guardado)
    }

    public static void guardarTema(Context contexto, int tema){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);
        preferencias.edit().putInt(TEMA,tema).apply();
    }

    public static int getTema(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);
        return preferencias.getInt(TEMA, Temas.THEME_DEFAULT);//Recuperar el tema del usuario (por defecto "Claro" por si nunca se ha guardado)
    }

    public static void guardarIdioma(Context contexto, String idioma){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);
        preferencias.edit().putString(IDIOMA,idioma).apply();
    }

    public static String getIdioma(Context contexto){
        SharedPreferences preferencias = contexto.getSharedPreferences(ID_PREFERENCIAS,MODE_PRIVATE);
        return preferencias.getString(IDIOMA, Idiomas.IDIOMA_DEFECTO);//Recuperar el idioma del usuario (por defecto "español" por si nunca se ha guardado)
    }
}
