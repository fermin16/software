package com.example.PampApp;

/**
 * Interfaz de idioma: contiene los Strings con los nombres de los idiomas de la aplicación que se
 * utilizarán en varios Activities.
 */
public interface Idiomas {
    public static final String IDIOMA_DEFECTO = "es";
    public static final String IDIOMA_INGLES = "en";
}
