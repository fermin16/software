package com.example.PampApp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PampApp.Modelos.Login;
import com.example.PampApp.Modelos.Preferencias;
import com.example.PampApp.Modelos.User;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.regex.Pattern;

/**
 * Clase LoginActivity: esta clase muestra una interfaz con la que el usuario puede iniciar sesion
 * mediante correo electronico y contraseña. Tambien da la opcion al usuario de registrarse.
 */
public class LoginActivity extends AppCompatActivity{

    private UserLoginTask mAuthTask = null; //Campo que permite llevar un track de la actividad login por si queremos cancelarla
    private Login myLogin; //Objeto de tipo Login para el inicio de sesion
    private static final int SHOW_SUBACTIVITY = 1; //Campo para llamar a otras actividades al pulsar el boton iniciar sesion o registarse
    private boolean estadoBoton;//Variable que contiene el esyado actual del boton recuerdame

    //Campos para la interfaz de usuario.
    private AutoCompleteTextView mEmailView; //Campo del email
    private EditText mPasswordView; //Campo de la password
    private View mProgressView; //Campo para la animacion de la progress bar
    private View mLoginFormView;
    private TextView LoginError; //Campo para mostrar errores en el inicio de sesion
    private Connection connect; //campo conexion
    private Button logInButton;
    private Button signInButton;
    private RadioButton recuerdame; //Radio buton para inicio de sesión automatico

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        connect = (Connection)getApplicationContext(); //Inicializar la conexion con el servidor

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);

        /*Establecer la accion que se realizara al pulsar intro en el teclado con el campo de
        texto seleccionado*/
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        logInButton = (Button) findViewById(R.id.LogInbutton);
        logInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //Llamar a la función encargada de hacer el intento de login
                attemptLogin();
            }
        });

        signInButton = (Button) findViewById(R.id.SignInButton);
        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getApplicationContext(),SignInActivity.class);
                startActivityForResult(intent,SHOW_SUBACTIVITY); //Iniciar la Subactividad registrarse
            }
        });

        recuerdame = findViewById(R.id.Recuerdame);

        estadoBoton = recuerdame.isChecked(); //Guardar el estado actual del boton

        recuerdame.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(estadoBoton){
                    recuerdame.setChecked(false);
                }
                estadoBoton = recuerdame.isChecked();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        LoginError = (TextView) findViewById(R.id.LoginError);

        mEmailView.setText(Preferencias.getEmailUsuario(this));
        if(Preferencias.getEstadoBoton(this)){ //Comprobar si se marco el boton de inicio de sesion automatico:
            if(Preferencias.getPasswordUsuario(this).compareTo("")!= 0){
                mPasswordView.setText(Preferencias.getPasswordUsuario(this));
                estadoBoton = true;
                attemptLogin(); //Realizar inicio de sesion
            }
        }


    }

    /**
     * Este metodo se llama cuando el usuario intenta realizar un inicio de sesion, el metodo
     * comprueba cada uno de los campos introducidos y si son correctos entonces se realiza el
     * incio de sesion, si no se reporta un error al usuario.
     */
    private void attemptLogin() {
        //Ocultar el teclado (despues de pulsar el boton de iniciar sesion o despues de pulsar el boton ok del teclado):
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        if(getCurrentFocus() !=null)
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        if (mAuthTask != null) {
            return;
        }

        //Resetear los campos de error
        mEmailView.setError(null);
        mPasswordView.setError(null);

        //Recuperar los datos introducidos
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        //Comprobar que la contraseña no sea nula
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        //Comprobar que el email no sea nulo y que sea correcto
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            //Si ha habido algun error mostrarlo y hacer focus en el ultimo campo.
            focusView.requestFocus();
        } else {
            // Si ha ido bien mostrar la animacion del Spinner y ejecutar por debajo una AsyncTask
            // que contiene la logica de inicio de sesion.
            showProgress(true);
            myLogin = (Login) Login.create("Login");
            myLogin.setEmail(email);
            myLogin.setPassword(password);

            mAuthTask = new UserLoginTask();
            makeToast(getString(R.string.conectandoServidor),0);
            mAuthTask.execute((Void) null);
        }
    }

    /* Este metodo comprueba si una direccion de correo electronico es correcta de acuerdo a un
     * Pattern */
    private boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    /**
     * Este metodo se encarga de mostrar la animacion del spinner cuando un usuario intenta hacer
     * un inicio de sesion.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /*
     * Metodo para generar Toasts a partir de unos parametros de entrada.
     */
    private void makeToast(String text, int option){
        Toast.makeText(getApplicationContext(),text,option).show();
    }

    /**
     * Esta clase representa el proceso de login en la aplicacion, contiene la logica de inicio de
     * sesion. El proceso se realiza mediante una AsyncTask que se ejecuta por debajo del Task
     * que la ha llamdo y no modifica la vista en ningun momento.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        private final Login login; //Objeto Login que contiene la informacion del intento de inicio de sesion.
        private List<ParseObject> query_result; //Campo que almacena el resultado de la query
        private Semaphore semaforo; //Campo semaforo neceario para que el padre espere a que se actualicen los datos.
        private Boolean hay_conexion; //Campo para comprobar si hay conexion o no.
        private ParseObject usuario; //Campo para almacenar los datos del usuario que ha iniciado sesion.

        public UserLoginTask() {
            login = myLogin;
            query_result = new ArrayList<>();
            semaforo = new Semaphore(0);
            hay_conexion = true;
        }

        /* Metodo que e encarga de almacenar el resultado de la query en el campo query_result
        * para poder trabajar con el despues de realizar la query.*/
        private void saveList(List<ParseObject> result){
            query_result.addAll(result);
        }

        /* Metodo que modifica el semaforo añadiendole un permiso para que el padre pueda continuar
         * despues de que el hijo haya hecho la query. */
        private void libera_semaforo(){
            semaforo.release(); //Dar permiso al padre para que continue
        }

        /* Metodo que modifica el campo hay_conexion cuando no hay conexion y por tanto no se puede hacer
         * la query. */
        private void sinConexion(){
            hay_conexion = false;
        }

        /* Este metodo se llamara automaticamente al hacer un execute del AsyncTask. Contiene la
        * logica de inicio de sesion. El metodo realiza una query e intenta obtener un objeto de la
        * BBDD que contenga un campo email que coincida con el del objeto login, es decir con el
        * que el usuario ha introducido, en caso de que haya coincidencia se comprueba la contraseña
        * del objeto obtenido de la query con la del objeto login y si coinciden el metodo devuelve
        * true, en caso de que no haya conexion o uno de los campos del objeto login no coincidan
        * se informará al usuario y se devolvera false.*/
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Boolean doInBackground(Void... params) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
            query.whereEqualTo(User.EMAIL, login.getEmail());

            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> loginList, ParseException e) {
                    if (e == null) {
                        saveList(loginList); //Guardar el resultado de la query
                    } else {
                        LoginError.setText(R.string.error_conexion);
                        makeToast(getString(R.string.error_conexion),0);
                        sinConexion(); //No hay conexion
                    }
                    libera_semaforo(); //Liberar el semaforo para que el padre continue
                }
            });
            try { //Esperar a que el hijo nos devuelva la lista
                semaforo.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(hay_conexion) {
                if (!query_result.isEmpty()) {
                    ParseObject obj = query_result.get(0); //Obtener el resultado de la query
                    if (obj.getString(User.PASSWORD).compareTo(login.getPassword()) == 0) { //Coincide la password
                        usuario = obj;
                        return true;
                    }
                }
                LoginError.setText(R.string.login_error);
            }
            return false;
        }

        /**
         * Este metodo se ejecuta automaticamente despues de doInBackground, obtiene como parametro
         * el valor devuelto por el metodo anterior y dependiendo de este valor, iniciara sesion en
         * la app (si ha devuelto true) o reportara un error al usuario (si ha devuelto false).
         * */
        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                guardarPreferencias();
                Intent siguiente=new Intent(getApplicationContext(), pantallaPrincipal.class);
                siguiente.putExtra("user",myLogin.getEmail());
                siguiente.putExtra("Actividad","Login");
                siguiente.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(siguiente);
                finish();
            } else {
                LoginError.setVisibility(TextView.VISIBLE);
                makeToast(getString(R.string.inicioIncorrecto),0);
            }
        }

        private void guardarPreferencias(){
            Preferencias.guardaEstadoBoton(getApplicationContext(),estadoBoton);//Guardar el estado del boton recuerdame
            //Comprobar si la ultima persona que hizo inicio de sesion es la misma que la que intenta hacer ahora:
            if(Preferencias.getEmailUsuario(getApplicationContext()).compareTo((String) usuario.get(User.EMAIL)) != 0){
                //Si no es asi guardar temas por defecto.
                Preferencias.guardarTema(getApplicationContext(),Temas.THEME_DEFAULT);
                Preferencias.guardarIdioma(getApplicationContext(),Idiomas.IDIOMA_DEFECTO);
            }
            Preferencias.guardarEmailUsuario(getApplicationContext(), (String) usuario.get(User.EMAIL));//Guardar el usuario que inicia sesion
            Preferencias.guardarNombreUsuario(getApplicationContext(), (String) usuario.get(User.NOMBRE));
            Preferencias.guardarAp1Usuario(getApplicationContext(), (String) usuario.get(User.AP1));
            Preferencias.guardarAp2Usuario(getApplicationContext(), (String) usuario.get(User.AP2));
            Preferencias.guardarDNIUsuario(getApplicationContext(), String.valueOf(usuario.get(User.DNI)));
            Preferencias.guardarPasswordUsuario(getApplicationContext(),(String) usuario.get(User.PASSWORD));
        }
    }
}

