package com.example.PampApp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.PampApp.Modelos.ElementoListaAlerta;
import com.example.PampApp.Modelos.Preferencias;

import java.util.ArrayList;

public class ListaAdapterAlert extends BaseAdapter implements  Temas{
    private ArrayList<ElementoListaAlerta> lista;
    private Context context;

    public ListaAdapterAlert(ArrayList<ElementoListaAlerta> lista, Context context) {
        this.lista = lista;
        this.context = context;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ElementoListaAlerta elemento = (ElementoListaAlerta) getItem(position);
        convertView = LayoutInflater.from(context).inflate(R.layout.elem_lista_alerta,null);
        Bitmap bmp= BitmapFactory.decodeByteArray(elemento.getScaledData(),0,elemento.getScaledData().length);
        ImageView foto= (ImageView)convertView.findViewById(R.id.foto);
        TextView titulo =(TextView )convertView.findViewById(R.id.titulo);
        TextView direccion =(TextView )convertView.findViewById(R.id.direccion);
        //Cambiar el color del texto de las alertas en funcion del tema:
        int temaActual = Preferencias.getTema(context);
        if(temaActual == THEME_DARK){
            titulo.setTextColor(context.getResources().getColor(R.color.textColorDark));
            direccion.setTextColor(context.getResources().getColor(R.color.textColorDark));
        }
        else if(temaActual == THEME_DEFAULT){
            titulo.setTextColor(context.getResources().getColor(R.color.textColorDefault));
            direccion.setTextColor(context.getResources().getColor(R.color.textColorDefault));
        }
        else if(temaActual == THEME_BLUE_LIGHT || temaActual == THEME_BLUE_DARK){
            titulo.setTextColor(context.getResources().getColor(R.color.appBarAzul));
            direccion.setTextColor(context.getResources().getColor(R.color.appBarAzul));
        }
        else if(temaActual == THEME_RED_LIGHT || temaActual == THEME_RED_DARK){
            titulo.setTextColor(context.getResources().getColor(R.color.appBarRoja));
            direccion.setTextColor(context.getResources().getColor(R.color.appBarRoja));
        }
        foto.setImageBitmap(bmp);
        titulo.setText(elemento.getTitulo());
        direccion.setText(elemento.getDireccion());
        return convertView;
    }
}
