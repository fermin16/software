package com.example.PampApp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class verFoto extends AppCompatActivity {
    private ImageView imagen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_foto);
        imagen = findViewById(R.id.fotoGrande);
        Bitmap bmp = getIntent().getParcelableExtra("foto");
        imagen.setImageBitmap(bmp);
    }
}
