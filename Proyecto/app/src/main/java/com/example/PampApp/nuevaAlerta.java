package com.example.PampApp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PampApp.Modelos.Alert;
import com.example.PampApp.Modelos.Solucion;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.parse.ParseGeoPoint;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class nuevaAlerta extends AppCompatActivity implements PermissionsListener {

    //variables para la funcionalidad
    private Double latitud;
    private Double longitud;
    private Geocoder geocoder;
    private PermissionsManager permissionsManager;
    private final int ACTIVAR_UBICACION = 0; //Codigo de la subactividad que nos servira para saber si el usuario a activado o no la ubicacion
    private final int CAMARA = 1; //Codigo para el resultado de la subactividad camara

    //Campos para los elementos graficos
    private ImageView foto;
    Drawable checkFoto; //campo para saber si se ha tomado una foto
    Bitmap bitmap; //bitmap para base de datos
    private Button tomarFoto;
    private EditText titulo;
    private EditText descripcion;
    private RadioGroup opccion;
    private EditText direccion;
    private RatingBar rate;
    private ImageButton actualizar;
    private ProgressBar progressBar;
    private EditText solucion;
    private String user;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Ajustes.onActivityCreateSetAjustes(this,0);
        geocoder = new Geocoder(this);

        //Inicializar el contenido grafico:
        setContentView(R.layout.activity_nueva_alerta);

        user = getIntent().getStringExtra("user");
        if(user==null){
            Intent inicio=new Intent(getApplicationContext(), LoginActivity.class);
            Toast.makeText(getApplicationContext(),getString(R.string.usuarioNoDetectado),Toast.LENGTH_LONG).show();
            startActivity(inicio);
        }
        tomarFoto = (Button) findViewById(R.id.tomarFoto);
        foto = (ImageView) findViewById(R.id.fotoAlerta);
        checkFoto= foto.getDrawable();
        titulo = (EditText) findViewById(R.id.titulo);
        descripcion = (EditText) findViewById(R.id.descripcion);
        direccion = (EditText) findViewById(R.id.direccionDato);
        rate = (RatingBar) findViewById(R.id.ratingBar);
        rate.setRating(2);
        opccion = (RadioGroup) findViewById(R.id.opcionUbi);
        progressBar = findViewById(R.id.progressBarAlerta);
        solucion= findViewById(R.id.soluciones);
        solucion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        actualizar = (ImageButton) findViewById(R.id.actualizarUbicacion);
        actualizar.setEnabled(false); //Desactivamos el boton

        //Establecer el comportamiento de los radio buttons
        opccion.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.ubiAuto) {
                    direccion.setVisibility(View.INVISIBLE);
                    showProgress(true);
                    obtenerUbicacion();
                    direccion.setEnabled(false);
                    actualizar.setEnabled(true);
                    showProgress(false);
                } else {
                    direccion.setVisibility(View.VISIBLE);
                    direccion.setEnabled(true);
                    //Poner la latitud y longitud a null por si el usuario detecta su ubicacion y luego la cambia
                    latitud = null;
                    longitud = null;
                    actualizar.setEnabled(false);
                }
            }
        });

        //Establecer el comportamiento del boton actualizar
        actualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                obtenerUbicacion();
                showProgress(false);
            }
        });

        //Intentar obtener datos de una actividad llamante, en caso de que el usuario cree la alerta desde el mapa:
        Intent intent = getIntent();
        if(intent != null) {
            if(intent.hasExtra(String.valueOf(R.string.direccion_alerta))){ //Comprobar que esta uno de los campos que envia el mapa
                latitud = intent.getDoubleExtra(String.valueOf(R.string.latitud_alerta), 0);
                longitud = intent.getDoubleExtra(String.valueOf(R.string.longitud_alerta), 0);
                String direc = intent.getStringExtra(String.valueOf(R.string.direccion_alerta));
                direccion.setText(direc);
                direccion.setEnabled(false);
                opccion.clearCheck(); //Quitar el check a todos los botones de manera que si un usario decide modificar la direccion el progrma la compruebe.
            }
        }

    }

    /* Metodo que determina la accion que se llevara acabo cuando el usuario pulsa el boton volver
     * atras. */
    @Override
    public void onBackPressed() {
        dialogoCancelar(null);
    }

    public void dialogo(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogCustom);
        builder.setTitle(R.string.valoraciones);
        builder.setMessage(getString(R.string.infoAlerta));
        builder.setNegativeButton(R.string.aceptar, null);
        Dialog dialogo = builder.create();
        dialogo.show();
    }

    public void dialogoCancelar(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,Temas.THEME_DIALOG);
        builder.setTitle(R.string.Advertencia);
        builder.setMessage(R.string.descartarAlerta);
        builder.setNegativeButton(R.string.cancelar, null);
        builder.setPositiveButton(R.string.aceptar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        Dialog dialogo = builder.create();
        dialogo.show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.guardar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save) {
            crearAlerta();
        } else if (id == R.id.cancel) {
            dialogoCancelar(null);
        }

        return super.onOptionsItemSelected(item);
    }

    public void tomarFoto(View v) {
        Intent intento = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intento,CAMARA );
    }

    /* Metodo para chequear los resultados de las subactiviades:*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case ACTIVAR_UBICACION:
                obtenerUbicacion();
                break;
            case CAMARA:
                if(data != null) { //Comprobar si el intent esta vacio, por si el usuario ha vuelto atras
                    bitmap= (Bitmap) data.getExtras().get("data");
                    foto.setImageBitmap(bitmap);
                }
                break;
        }
    }

    /* Metodo para crear una alerta. Este metodo comprueba cada uno de los campos cuando el usuario
     *  intenta crear una alerta si tods son correctos la alerta se creara y se alamacenara en el
     *  servidor en caso contrario se informara a el usuario.*/
    private void crearAlerta() {
        Alert alerta;
        boolean error = false;
        String sTitulo = titulo.getText().toString();
        String sDescripcion = descripcion.getText().toString();
        String sDireccion = direccion.getText().toString();
        int puntuacion = (int) rate.getRating();

        if (foto.getDrawable()==checkFoto){
            Toast.makeText(this, getString(R.string.imagenNoEncontrada), Toast.LENGTH_SHORT).show();
            error=true;
        }else {
            if (sTitulo.isEmpty()) {
                error = true;
                titulo.setError(getString(R.string.tituloRequerido));
            }
            if (sDescripcion.isEmpty()) {
                error = true;
                descripcion.setError(getString(R.string.descripcionRequerida));
            }
            else if(sDescripcion.length() < this.getResources().getInteger(R.integer.minDescripcion)){
                error = true;
                descripcion.setError(getString(R.string.minCaracteresDescripcion)+this.getResources().getInteger(R.integer.minDescripcion));
            }
            if (sDireccion.isEmpty()) {
                error = true;
                direccion.setError(getString(R.string.direccionRequerida));
            } else if (latitud == null && longitud == null) { //En caso de que la direccion no venga del mapa comprorbar que es correcta:
                try {
                    List<Address> lista;
                    System.out.println(sDireccion);
                    lista = geocoder.getFromLocationName(sDireccion, 1);
                    if (!lista.isEmpty()) {
                        latitud = lista.get(0).getLatitude();
                        longitud = lista.get(0).getLongitude();
                    } else {
                        direccion.setError(getString(R.string.direccion_error));
                        Toast.makeText(this, R.string.direccion_error, Toast.LENGTH_SHORT).show();
                        error = true;
                    }
                } catch (IOException e) {
                    Toast.makeText(this, getString(R.string.comprobar_direccion), Toast.LENGTH_SHORT).show();
                    error = true;
                    direccion.setError(getString(R.string.comprobar_direccion));
                }
            }
        }
        if (!error) {
            alerta=new Alert();
            alerta.setTitulo(sTitulo);
            alerta.setDescripcion(sDescripcion);
            alerta.setDireccion(sDireccion);
            alerta.setlocalizacion(new ParseGeoPoint(latitud,longitud));
            alerta.setRate(puntuacion);
            alerta.setNumRate(1);
            alerta.setUser(user);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
            byte[] scaledData = stream.toByteArray();
            alerta.put("foto",scaledData);
            if(!solucion.getText().toString().isEmpty()) {
                ArrayList<Solucion> listaSoluciones = new ArrayList<Solucion>();
                Solucion sol = new Solucion();
                sol.setSolucion(solucion.getText().toString());
                sol.setUser(user);
                listaSoluciones.add(sol);
                alerta.put("solucion", listaSoluciones);
            }
            alerta.saveInBackground();
            //Imprimir toast personalizado
            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.toast_xml,
                    (ViewGroup)findViewById(R.id.toastLayout));
            TextView toastText = (TextView) view.findViewById(R.id.toastText);
            toastText.setText(getString(R.string.alertaCorrecta));
            Toast toast = new Toast(this);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(view);
            toast.setGravity(Gravity.CLIP_HORIZONTAL, 0, 0);
            toast.show();
            finish();
        }
    }

    /* Metodo que chequea los permisos de ubicacion, tras esto chequea que los servicios de red y
     de ubicacion esten activos y en caso de estarlo intenta obtener la ultima ubicacion del usuario.
     */
    @SuppressLint("MissingPermission")
    private void obtenerUbicacion(){
        //Comprobar si se han concedido los permisos de ubicacion:
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            if(checkLocationServices()){//Comprobar si estan activados los servicios de red y ubicacion.
                Location loc = getLastKnownLocation(); //Obtener la ubicacion
                if (loc != null) {
                    latitud = loc.getLatitude();
                    longitud = loc.getLongitude();
                    try { //Obtener la direccion, printearla en el cuadro de texto y mostrarla
                        List<Address> address = geocoder.getFromLocation(latitud, longitud, 1);
                        if (!address.isEmpty()) {
                            direccion.setText(address.get(0).getAddressLine(0));
                            direccion.setVisibility(View.VISIBLE);
                        }
                    } catch (IOException e) { //En caso de que el usuario no tenga cnoexion a internet:
                        Toast.makeText(this, getString(R.string.errorUbicacion), Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(this,getString(R.string.errorUbicacion),Toast.LENGTH_SHORT).show();
                }
            }
        }
        else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, getString(R.string.user_location_permission_explanation), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            obtenerUbicacion();
        } else {
            Toast.makeText(this, getString(R.string.user_location_permission_not_granted), Toast.LENGTH_LONG).show();
        }
    }

    /* Metodo que permite chequear los servicios de acceso para ver si estan activados o no. Devuelve
     *  falso si estan desactivados y verdadero en caso contrario.*/
    public  boolean checkLocationServices(){
        LocationManager lm = (LocationManager)this.getSystemService(this.LOCATION_SERVICE);
        boolean gps = false;
        boolean network = false;

        try {
            gps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}
        if(!gps){
            //Notificar al usuario que tiene desactivado el gps.
            new AlertDialog.Builder(this,Temas.THEME_DIALOG)
                    .setMessage(R.string.gps_desactivado)
                    .setPositiveButton(R.string.activar_gps, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),ACTIVAR_UBICACION);
                        }
                    }).setNegativeButton(R.string.cancelar_gps, null)
                    .show();
            return false;
        }
        else if(!network){
            //Notificar al usuario que tiene desactivado el gps.
            new AlertDialog.Builder(this,Temas.THEME_DIALOG)
                    .setMessage(R.string.net_desactivada)
                    .setPositiveButton(R.string.activar_gps, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),ACTIVAR_UBICACION);
                        }
                    }).setNegativeButton(R.string.cancelar_gps, null)
                    .show();
            return false;
        }
        return true;
    }

    /*Metodo que obtiene la ultima ubicacion del usuario, para ello comprueba cada uno de los proveedores
     * ubicacion y se queda con aquel que sea mas preciso. Por ultimo devulve la ubicacion del usuario */
    @SuppressLint("MissingPermission")
    private Location getLastKnownLocation() {
        LocationManager locationManager = (LocationManager)getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true); //Obtener solo los proveedores activos
        Location bestLocation = null; //Variable que almacena la mejor ubicacion
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    /**
     * Este metodo se encarga de mostrar la animacion del spinner cuando un usuario le da a recargar
     * ubicacion
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_longAnimTime);

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
