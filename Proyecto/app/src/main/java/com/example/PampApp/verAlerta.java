package com.example.PampApp;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PampApp.Modelos.Alert;
import com.example.PampApp.Modelos.Solucion;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;


public class verAlerta extends AppCompatActivity {
    private static final int SOLUCIONCODE = 1;
    private TextView titulo;
    private ImageView imagen;
    private TextView descripcion;
    private TextView direccion;
    private Button botonValorar;
    private TextView infoRate;
    private RatingBar rate;
    private String user;
    private String id;
    private ArrayList<Solucion> listaSoluciones;
    private boolean editable;
    private boolean eliminado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Ajustes.onActivityCreateSetAjustes(this, 0);
        setContentView(R.layout.activity_ver_alerta);

        id = getIntent().getStringExtra("id");
        user = getIntent().getStringExtra("user");
        listaSoluciones = new ArrayList<Solucion>();

        titulo = findViewById(R.id.titulo);
        imagen = findViewById(R.id.fotoAlerta);
        descripcion = findViewById(R.id.descripcion);
        direccion = findViewById(R.id.direccionDato);
        botonValorar = findViewById(R.id.buttonValorar);
        rate = findViewById(R.id.ratingBar);
        infoRate=findViewById(R.id.inforate);

        eliminado=false;

        final ParseQuery<ParseObject> query;
        query = ParseQuery.getQuery("Alert");
        query.whereEqualTo("objectId", id);
        query.include("solucion");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    try {
                        Alert alerta = (Alert) query.getFirst();
                        if (alerta.getUser().equals(user)) {
                            editable = true;
                        } else editable = false;
                        titulo.setText(alerta.getTitulo());
                        final byte[] foto = (byte[]) alerta.get("foto");
                        final Bitmap bmp = BitmapFactory.decodeByteArray(foto, 0, foto.length);
                        imagen.setImageBitmap(bmp);
                        imagen.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent verFoto= new Intent(getApplicationContext(), com.example.PampApp.verFoto.class);
                                verFoto.putExtra("foto",bmp);
                                startActivity(verFoto);
                            }
                        });

                        descripcion.setText(alerta.getDescripcion());
                        direccion.setText(alerta.getDireccion());
                        rate.setRating((float) alerta.getRate());
                        if (alerta.getList("solucion") != null) {
                            listaSoluciones.addAll(Objects.requireNonNull(alerta.<Solucion>getList("solucion")));
                        }
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gestion_alerta, menu);
        return super.onCreateOptionsMenu(menu);
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        int id2 = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id2== R.id.delete) {
            if(editable){
                final ParseQuery<ParseObject> query;
                query = ParseQuery.getQuery("Alert");
                query.whereEqualTo("objectId", id);
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if (e == null) {
                            Alert alerta = null;
                            try {
                                alerta = (Alert) query.getFirst();
                            } catch (ParseException e1) {
                                e1.printStackTrace();
                            }
                            if (alerta!=null)
                                    alerta.deleteInBackground();
                        }
                        eliminado=true;
                        finish();
                    }
                });
            }else{
                Toast.makeText(this,"No tiene permisos para elminar la alerta",Toast.LENGTH_SHORT).show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    public void solucione(View view) {
        Intent intent = new Intent(this, Soluciones.class);
        intent.putExtra("user", user);
        intent.putExtra("solucion", listaSoluciones);
        startActivityForResult(intent, SOLUCIONCODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 1) {
            listaSoluciones.clear();
            listaSoluciones.addAll((Collection<? extends Solucion>) data.getSerializableExtra("solucion"));
            Actualizar();
        }
    }

    private void Actualizar() {
        final ParseQuery<ParseObject> query;
        query = ParseQuery.getQuery("Alert");
        query.whereEqualTo("objectId", id);
        query.include("solucion");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    try {
                        Alert alerta = (Alert) query.getFirst();
                        alerta.put("solucion", listaSoluciones);
                        alerta.saveInBackground();
                    } catch (ParseException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
    }

    public void valorar(View view) {
        if (botonValorar.isEnabled()) {
            final ParseQuery<ParseObject> query;
            query = ParseQuery.getQuery("Alert");
            query.whereEqualTo("objectId", id);
            botonValorar.setEnabled(false);
            botonValorar.setClickable(false);
            botonValorar.setVisibility(View.INVISIBLE);
            rate.setEnabled(false);
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> objects, ParseException e) {
                    if (e == null) {
                        try {
                            Alert alerta = (Alert) query.getFirst();
                            double auxRate ;
                            auxRate = ((alerta.getRate() * alerta.getNumRate()) + rate.getRating()) / (alerta.getNumRate() + 1);
                            alerta.setRate((int) auxRate);
                            alerta.setNumRate(alerta.getNumRate() + 1);
                            rate.setRating((float) alerta.getRate());
                            infoRate.setText(Integer.toString(alerta.getRate()));
                            alerta.saveInBackground();
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            });
        }
    }
    public void dialogo(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogCustom);
        builder.setTitle(R.string.valoraciones);
        builder.setMessage(getString(R.string.infoAlerta));
        builder.setNegativeButton(R.string.aceptar, null);
        Dialog dialogo = builder.create();
        dialogo.show();
    }

    public void finish(){
        Intent resultado =new Intent();
        if (eliminado) {
            setResult(1, resultado);
        }else
            setResult(0);
        super.finish();
    }

}
