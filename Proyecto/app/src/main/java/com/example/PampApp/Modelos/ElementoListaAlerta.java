package com.example.PampApp.Modelos;

public class ElementoListaAlerta {
    private byte[] scaledData;
    private String titulo;
    private String direccion;
    private String id;

    public ElementoListaAlerta(byte[] scaledData, String titulo, String direccion, String id) {
        this.scaledData = scaledData;
        this.titulo = titulo;
        this.direccion = direccion;
        this.id=id;
    }

    public byte[] getScaledData() {
        return scaledData;
    }

    public void setScaledData(byte[] scaledData) {
        this.scaledData = scaledData;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getId(){return id;}

    public void setId(String id){this.id=id;}
}
