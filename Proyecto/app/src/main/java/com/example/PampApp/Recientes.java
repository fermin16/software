package com.example.PampApp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.PampApp.Modelos.ElementoListaAlerta;
import com.example.PampApp.Modelos.Solucion;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Recientes extends AppCompatActivity {
    private Geocoder geocoder;
    //variables lista
    private ListView lista;
    ArrayList<ElementoListaAlerta> alertas;
    private View mProgressView;
    String user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Ajustes.onActivityCreateSetAjustes(this,0);
        setContentView(R.layout.activity_recientes);
        user=getIntent().getStringExtra("user");
        lista = (ListView) findViewById(R.id.listaCercanas);
        alertas = new ArrayList<>();
        mProgressView = findViewById(R.id.indeterminateBar);
        showProgress(true);
        actualizarLista();
        lista.setAdapter(new ListaAdapterAlert(alertas,this));
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent verAlerta = new Intent(view.getContext(), com.example.PampApp.verAlerta.class);
                verAlerta.putExtra("user", user);
                verAlerta.putExtra("id", alertas.get(position).getId());
                startActivityForResult(verAlerta,1);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 1) {
            actualizarLista();
        }
    }



    public void actualizarLista() {
        ParseQuery<ParseObject> query;
        geocoder = new Geocoder(getApplicationContext());
        query = ParseQuery.getQuery("Alert");
        query.orderByDescending("_created_at");
        query.setLimit(30);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> query, ParseException e) {
                final ArrayList<ElementoListaAlerta> auxAlertas = new ArrayList<>();
                if (e == null) {
                    for (ParseObject obj : query) {
                        String titulo = obj.get("titulo").toString();
                        byte[] foto = (byte[]) obj.get("foto");
                        String id = obj.getObjectId();
                        ParseGeoPoint auxlocalizacion = obj.getParseGeoPoint("localizacion");
                        String direccion;
                        try { //Obtener la direccion, printearla en el cuadro de texto y mostrarla
                            List<Address> address = geocoder.getFromLocation(auxlocalizacion.getLatitude(), auxlocalizacion.getLongitude(), 1);
                            if (!address.isEmpty()) {
                                direccion = address.get(0).getAddressLine(0);
                            } else {
                                direccion = getString(R.string.direccionNoEncontrada);
                            }
                        } catch (IOException u) { //En caso de que el usuario no tenga cnoexion a internet:
                            direccion = getString(R.string.direccionNoEncontrada);
                        }
                        ElementoListaAlerta nuevo = new ElementoListaAlerta(foto, titulo, direccion,id);
                        auxAlertas.add(nuevo);
                    }
                    ListaAdapterAlert adpater = new ListaAdapterAlert(auxAlertas, getApplicationContext());
                    lista.setAdapter(adpater);
                    saveList(auxAlertas);
                    showProgress(false);
                }
                else{
                    Toast.makeText(getApplicationContext(),R.string.errorConexionServ,Toast.LENGTH_SHORT).show();
                    showProgress(false);
                    finish();
                }
            }
        });
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            lista.setVisibility(show ? View.GONE : View.VISIBLE);
            lista.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    lista.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            lista.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void saveList(List<ElementoListaAlerta> result) {
        alertas.addAll(result);
    }
}
