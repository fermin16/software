package com.example.PampApp.Modelos;

/**
 * Clase Login:
 * Esta clase contiene la definicion de objetos de tipo login que extienden del servidor
 * y que seran utilizado para realizar consultas en la base de datos y poder iniciar sesion.
 */

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Login")
public class Login extends ParseObject {

    public Login(){
    }

    public void setEmail(String email){
        put("email",email);
    }

    public void setPassword(String password){
        put("password",password);
    }

    public String getEmail(){
        return getString("email");
    }

    public String getPassword(){
        return getString("password");
    }
}
