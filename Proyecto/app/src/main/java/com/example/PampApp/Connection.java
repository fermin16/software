package com.example.PampApp;

import android.app.Application;

import com.example.PampApp.Modelos.Alert;
import com.example.PampApp.Modelos.Login;
import com.example.PampApp.Modelos.Solucion;
import com.example.PampApp.Modelos.User;
import com.parse.Parse;
import com.parse.ParseObject;

public class Connection extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        ParseObject.registerSubclass(Login.class);
        ParseObject.registerSubclass(User.class);
        ParseObject.registerSubclass(Alert.class);
        ParseObject.registerSubclass(Solucion.class);


        Parse.initialize(new Parse.Configuration.Builder(getApplicationContext())
                .applicationId(getString(R.string.app_id)) //si no has cambiado APP_ID, sino pon el valor de APP_ID
                .clientKey("empty")
                .server(getString(R.string.server_url))   // '/' importante after 'parse'
                .build());
    }
}
