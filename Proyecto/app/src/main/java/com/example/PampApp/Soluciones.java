package com.example.PampApp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.PampApp.Modelos.Solucion;

import java.util.ArrayList;
import java.util.List;

public class Soluciones extends AppCompatActivity {

    private ArrayList<Solucion> soluciones;
    private ListView lista;
    private List<String> auxSolucion;
    private LinearLayout edit;
    private TextView solEdit;
    private Button b1;
    private Button b2;
    private String user;
    ArrayAdapter<String> adaptador;
    private boolean modificado=false;
    private boolean vacio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Ajustes.onActivityCreateSetAjustes(this, 1);
        setContentView(R.layout.activity_soluciones);
        soluciones = (ArrayList<Solucion>) getIntent().getSerializableExtra("solucion");
        auxSolucion = new ArrayList<String>();
        lista = findViewById(R.id.listaSoluciones);

        user = getIntent().getStringExtra("user");

        if (soluciones != null && soluciones.size() > 0) {
            for (Solucion sol : soluciones) {
                auxSolucion.add(sol.getSolucion());
            }
        } else {
            vacio=true;
            auxSolucion.add("No se han encontrado Soluciones");
        }

        adaptador = new ArrayAdapter<String>(this, R.layout.layout, R.id.soluciontext, auxSolucion);
        lista.setAdapter(adaptador);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    dialogo2(view,position);
                    lista.setAdapter(adaptador);
                }
        });
    }

    public void finish(){
        Intent resultado =new Intent();
        if (modificado) {
            resultado.putExtra("solucion",soluciones);
            setResult(1, resultado);
        }else
            setResult(0);
        super.finish();
    }

    public void dialogo(View v) {
        final Solucion nueva=new Solucion();
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogCustom);
        builder.setTitle("Escriba su solucion");
        final EditText imput= new EditText(this);
        imput.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(imput);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!imput.getText().toString().isEmpty()) {
                    if(vacio){
                        adaptador.remove(auxSolucion.get(0));
                        vacio=false;
                    }
                    nueva.setSolucion(imput.getText().toString());
                    nueva.setUser(user);
                    soluciones.add(nueva);
                    adaptador.add(imput.getText().toString());
                    modificado = true;
                }
            }
        });
        builder.setNegativeButton("Cancelar",null);
        Dialog dialogo = builder.create();
        dialogo.show();
    }
    public void dialogo2(View v, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialogCustom);
        builder.setTitle("Desea eliminar esta solucion?");
        final TextView imput= new TextView(this);
        imput.setText(auxSolucion.get(position));
        builder.setView(imput);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                soluciones.remove(position);
                adaptador.remove(auxSolucion.get(position));
                modificado=true;
            }
        });
        builder.setNegativeButton("Cancelar",null);
        Dialog dialogo = builder.create();
        dialogo.show();
    }

    public void volver(View view){
        finish();
    }
}
