package com.example.PampApp;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PampApp.Modelos.Preferencias;

public class pantallaPrincipal extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, Temas {
    private String user;
    private static final int AJUSTES = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Obtener el tema actual para mostrarlo y mostrar el layout correspondiente al tema:
        int layout_actual= R.layout.activity_pantalla_principal;
        int temaActual = Preferencias.getTema(this);
        if(temaActual != THEME_DEFAULT){
            if(temaActual == THEME_DARK) {
                layout_actual = R.layout.activity_pantalla_principal;
            }
            else if(temaActual == THEME_BLUE_LIGHT || temaActual == THEME_BLUE_DARK) {
                layout_actual = R.layout.activity_pantalla_principal_azul;
            }
            else if(temaActual == THEME_RED_LIGHT || temaActual == THEME_RED_DARK) {
                layout_actual = R.layout.activity_pantalla_principal_roja;
            }
            Ajustes.temaSeleccionado = temaActual;
        }
        else{
            Ajustes.temaSeleccionado = THEME_DEFAULT;
        }
        //Obteener el idioma almacenado y aplicarlo
        Ajustes.idiomaSeleccionado = Preferencias.getIdioma(this);
        Ajustes.onActivityCreateSetAjustes(this,1);
        setContentView(layout_actual);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        user=intent.getStringExtra("user");
        if (user==null){
            Toast.makeText(getApplicationContext(),R.string.usuarioNoDetectado,Toast.LENGTH_LONG).show();
            cerrarSesion(this);
        }

        //Si la actividad ha sido invocada por el login mostar los mensajes de bienvenida:
        if(intent.hasExtra("Actividad")){
            Toast.makeText(this,getString(R.string.inicioCorrecto),Toast.LENGTH_SHORT).show();
            Toast.makeText(this,getString(R.string.mensajeBienvenida)+" "+ Preferencias.getNombreUsuario(getApplicationContext())+" "+Preferencias.getAp1Usuario(getApplicationContext())+" "+Preferencias.getAp2Usuario(getApplicationContext()),Toast.LENGTH_SHORT).show();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            dialogCerrarSesion(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menuadd, menu);
        //Rellenar la info del slide de la pantalla principal:
        TextView nombre =  findViewById(R.id.nombreCompletoUsuario);
        TextView correo =  findViewById(R.id.correoUsuario);
        nombre.setText(getString(R.string.hola)+" "+Preferencias.getNombreUsuario(this)+" "+Preferencias.getAp1Usuario(this)+" "+Preferencias.getAp2Usuario(this));
        correo.setText(user);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cerrarSesion) {
            dialogCerrarSesion(this);
        }else if(id==R.id.nuevaAlerta){
            nuevaAlerta(null);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.ajustesApp) {
            Intent intent = new Intent(getApplicationContext(), Ajustes.class);
            startActivityForResult(intent,AJUSTES);
        } else if (id == R.id.miCuenta) { //Actividad mi_cuenta:
            Intent intent = new Intent(getApplicationContext(), miCuenta.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void alertasRecientes(View view) {
        Intent intent = new Intent(getApplicationContext(), Recientes.class);
        intent.putExtra("user",user);
        startActivity(intent);
    }

    public void alertasCercanas(View view) {
        Intent intent = new Intent(getApplicationContext(), cercanas.class);
        intent.putExtra("user",user);
        startActivity(intent);
    }

    public void misAlertas(View view) {
        Intent intent = new Intent(getApplicationContext(), misAlertas.class);
        intent.putExtra("user",user);
        startActivity(intent);
    }
    public void nuevaAlerta(View view) {
        Intent intent = new Intent(getApplicationContext(), nuevaAlerta.class);
        intent.putExtra("user",user);
        startActivity(intent);
    }

    public void mapa(View view) {
        Intent intent = new Intent(getApplicationContext(), Map.class);
        intent.putExtra("user",user);
        startActivity(intent);
    }

    public void dialogCerrarSesion(final Activity activity){
        new AlertDialog.Builder(this,Temas.THEME_DIALOG)
                .setTitle(R.string.cerrar_sesion)
                .setMessage(R.string.peticion_cerrar_sesion)

                // Specifying a listener allows you to take an action before dismissing the dialog.
                // The dialog is automatically dismissed when a dialog button is clicked.
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        cerrarSesion(activity);
                    }
                })

                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /* Metodo para chequear los resultados de las subactiviades:*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case AJUSTES: //Comprobar si el usuario ha cambiado los ajustes.

                int temaActual = Preferencias.getTema(this);
                String idiomaActual = Preferencias.getIdioma(this);

                if(Ajustes.temaSeleccionado != temaActual){
                   //Generar el intent para cambiar ajustes:
                    Intent intent = new Intent(this,pantallaPrincipal.class);
                    intent.putExtra("user",user); //Añadir el user para evitar problemas al leer del intent

                    if(Ajustes.idiomaSeleccionado.compareTo(idiomaActual)!= 0) { //Si ha cambiado el idioma
                        Ajustes.cambiaAjustes(this,Ajustes.temaSeleccionado,Ajustes.idiomaSeleccionado,intent);
                        Preferencias.guardarIdioma(this,Ajustes.idiomaSeleccionado);
                    }
                    else //Si no ha cambiado el idioma
                        Ajustes.cambiaAjustes(this,Ajustes.temaSeleccionado,idiomaActual,intent);
                    Preferencias.guardarTema(this,Ajustes.temaSeleccionado);
                }
                else if(Ajustes.idiomaSeleccionado.compareTo(idiomaActual)!= 0){ //Si ha cambiado el idioma y no el tema
                    Intent intent = new Intent(this,pantallaPrincipal.class);
                    intent.putExtra("user",user); //Añadir el user para evitar problemas al leer del intent

                    Ajustes.cambiaAjustes(this,temaActual,Ajustes.idiomaSeleccionado,intent);
                    Preferencias.guardarIdioma(this,Ajustes.idiomaSeleccionado);
                }
                break;
            default:
                break;
        }
    }

    /** Metodo que permite cerrar la sesión del usuario activo. Al cerrar sesión se eliminan datos como
     * la password, y el estado del botón recuerdame.**/
    public static void cerrarSesion(Activity activity){
        Toast.makeText(activity.getApplicationContext(),activity.getString(R.string.sesionCerrada),Toast.LENGTH_SHORT).show();
        Preferencias.guardaEstadoBoton(activity.getApplicationContext(),false);
        Preferencias.guardarPasswordUsuario(activity.getApplicationContext(),"");
        Ajustes.cambioIdioma(Idiomas.IDIOMA_DEFECTO,activity);
        Intent intent = new Intent(activity.getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu mimenu){
//        getMenuInflater().inflate(R.menu.menuadd,mimenu);
//        return true;
//    }
}
