package com.example.PampApp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PampApp.Modelos.User;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.regex.Pattern;

public class SignInActivity extends AppCompatActivity {

    private UserSigninTask mAuthTask = null; //Campo que nos permite llevar un track de la actividad login por si queremos cancelarla
    private User myUser; //Objeto de tipo User que utilizaremos para el registro de usuarios

    //Campos para la interfaz de usuario.
    private EditText nombre;
    private EditText ape1;
    private EditText ape2;
    private EditText dni;
    private EditText email;
    private EditText email_conf;
    private EditText password;
    private EditText password_conf;
    private View ProgressView;
    private View SigninFormView;
    private TextView signInError;
    private Button signin;
    private Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Intent intent = getIntent();

        //Inicializar los EditText:
        nombre = findViewById(R.id.nom_registro);
        ape1 = findViewById(R.id.ape1_registro);
        ape2 = findViewById(R.id.ape2_registro);
        dni = findViewById(R.id.dni);
        email = findViewById(R.id.email_registro);
        email_conf = findViewById(R.id.email_confirma_registro);
        password = findViewById(R.id.password_registro);
        password_conf = findViewById(R.id.password_confirma_registro);
        signInError = findViewById(R.id.SignInError);

        //Inicializar los botones:
        signin = findViewById(R.id.SignInButton_registro);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Ocultar el teclado despues de pulsar el boton de iniciar sesion:
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                //Llamar a la función encargada de hacer el registrar
                attemptSignin();
            }
        });

        cancel = findViewById(R.id.CancelButton);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Ocultar el teclado despues de pulsar el boton de iniciar sesion:
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                //Llamar a la función encargada de cancelar el registrar
                cancelSignin();
            }
        });

        //Inicilizar el formulario y la animacion:
        SigninFormView = findViewById(R.id.signin_form);
        ProgressView = findViewById(R.id.signin_progress);
    }

    /**
     * Este metodo se llama cuando el usuario intenta realizar registrarse, el metodo
     * comprueba cada uno de los campos introducidos y si son correctos entonces se realiza el
     * registro, si no se reporta un error al usuario.
     */
    private void attemptSignin() {
        if (mAuthTask != null) {
            return;
        }

        //Resetear los campos de error
        nombre.setError(null);
        ape1.setError(null);
        ape2.setError(null);
        dni.setError(null);
        email.setError(null);
        email_conf.setError(null);
        password.setError(null);
        password_conf.setError(null);

        //Obtener la informacion de los campos:
        String name = nombre.getText().toString();
        String ap1 = ape1.getText().toString();
        String ap2 = ape2.getText().toString();
        String Dni = dni.getText().toString();
        String mail = email.getText().toString();
        String mail_conf = email_conf.getText().toString();
        String passwd = password.getText().toString();
        String passwd_conf = password_conf.getText().toString();

        boolean error = false;
        View focusView = new View(this);

        //Comprobar que el campo name no sea nulo:
        if (TextUtils.isEmpty(name)) {
            nombre.setError(getString(R.string.error_field_required));
            focusView = nombre;
            error = true;
        }

        //Comprobar que el campo ap1 no sea nulo:
        if (TextUtils.isEmpty(ap1)) {
            ape1.setError(getString(R.string.error_field_required));
            focusView = ape1;
            error = true;
        }

        //Comprobar que el campo ap2 no sea nulo:
        if (TextUtils.isEmpty(ap2)) {
            ape2.setError(getString(R.string.error_field_required));
            focusView = ape2;
            error = true;
        }

        //Comprobar que el campo DNI no sea nulo:
        if (TextUtils.isEmpty(Dni)) {
            dni.setError(getString(R.string.error_field_required));
            focusView = dni;
            error = true;
        }
        else if(!isDniValid(Dni)){ //Comprobar que el campo DNI sea correcto:
            dni.setError(getString(R.string.dni_error));
            focusView = dni;
            error = true;
        }

        //Comprobar los emails:
        if (!checkEmails(mail,mail_conf,email,email_conf,this,focusView)){
            error = true;
        }

        //Comprobar las passwords:
        if (!checkpasswords(passwd,passwd_conf,password,password_conf,this,focusView)) {
            error = true;
        }

        if (error) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            signInError.setText(getString(R.string.compruebe_campos));
            signInError.setVisibility(TextView.VISIBLE);

        } else {
            signInError.setVisibility(TextView.INVISIBLE);
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            //Crear el objeto User para realizar el intento de registro
            //myLogin = (Login) Login.create("Login");
            myUser = (User) User.create("User");
            myUser.setNombre(name);
            myUser.setAp1(ap1);
            myUser.setAp2(ap2);
            myUser.setDni(Dni);
            myUser.setEmail(mail);
            myUser.setPassword(passwd);
            myUser.setPuntuacion(0);
            myUser.setVotos(0);

            mAuthTask = new UserSigninTask();
            makeToast(getString(R.string.conectandoServidor),0);
            mAuthTask.execute((Void) null);
        }
    }

    private void cancelSignin(){
        finish();
    }

    /*
     * Metodo que comprueba si un DNI es valido, de acuerdo a la longitud y a que este formado por
     * 8 digitos y una letra.
     */
    private boolean isDniValid(String Dni){
        if(Dni.length() == this.getResources().getInteger(R.integer.maxDNI)){ //Comprobar longitud es igual a 9
            //Comprobar que el ultimo caracter del DNI es una letra
            char[] dni_array = Dni.toCharArray(); //Pasar el DNI a array de caracteres
            if(Character.isLetter(dni_array[8])) {
                //Comprobar que el resto de caracteres sean digitos:
                int i = 0;
                while( i < this.getResources().getInteger(R.integer.maxDNI) - 1 && Character.isDigit(dni_array[i])){
                    i++;
                }
                if(i == this.getResources().getInteger(R.integer.maxDNI) -1)
                    return true;
            }
        }
        return  false;
    }

    /*
     * Metodo que comprueba los dos emails introducidos por el usuario.
     */
    public static boolean checkEmails(String mail,String mail_conf, EditText email, EditText email_conf, Context context, View focusView){
        if(!checkEmail(mail,email,context,focusView))
            return false;
        else if(!checkEmail(mail_conf,email_conf,context,focusView))
            return false;
        //Comprobar que emails coinciden:
        else if(mail.compareTo(mail_conf) != 0) {
            email.setError(context.getString(R.string.email_error));
            email_conf.setError(context.getString(R.string.email_error));
            focusView = email;
            return false;
        }
        return true;
    }

    /*
     * Metodo que comprueba si el mail introducidos por el usuario es valido:
     */
    public static boolean checkEmail(String mail, EditText email, Context context, View focusView) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        //Comprobar si el mail es vacio:
        if (TextUtils.isEmpty(mail)) {
            email.setError(context.getString((R.string.error_field_required)));
            focusView = email;
            return false;
        }
        //Comprobar que el mail es valido de acuerdo a un patron
        else if(!pattern.matcher(mail).matches()) {
            email.setError(context.getString(R.string.error_invalid_email));
            focusView = email;
            return false;
        }
        return true;
    }

    /*
     * Metodo que comprueba si las contraseñas introducidas por el usuario son validas.
     */
    public static boolean checkpasswords(String passwd, String passwd_conf, EditText password, EditText password_conf, Context context, View focusView){
        if(!checkPassword(passwd,password,context,focusView))
            return false;
        else if(!checkPassword(passwd_conf,password_conf,context,focusView))
            return false;
        //Comprobar que las passwords coinciden:
        else if(passwd.compareTo(passwd_conf) != 0) {
            password.setError(context.getString(R.string.password_error));
            password_conf.setError(context.getString(R.string.password_error));
            focusView = password;
            return false;
        }
        return true;
    }

    /*
     * Metodo que comprueba si la password introducida por el usuario es valida.
     */
    public static boolean checkPassword(String passwd, EditText password, Context context, View focusView) {
        //Comprobar si la password es vacia:
        if (TextUtils.isEmpty(passwd)) {
            password.setError(context.getString(R.string.error_field_required));
            focusView = password;
            return false;
        }
        //Comprobar el tamaño minimo de la password
        if(passwd.length() < context.getResources().getInteger(R.integer.minPassword)){
            password.setError(context.getString(R.string.passwd_length));
            focusView = password;
            return false;
        }
        //Comprobar que la password solo contiene letras y numeros
        else if (!checkPasswordChar(passwd.toCharArray())){
            password.setError(context.getString(R.string.passwd_caracteres));
            focusView = password;
            return false;
        }
        return true;
    }

    /** Funcion que compruba si una contraseña tien solo letras y numeros.**/
    public static boolean checkPasswordChar(char[] passwd_array){
        for(int i = 0; i < passwd_array.length; i++){
            if(!Character.isDigit(passwd_array[i])) {
                if(!Character.isLetter(passwd_array[i])) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Muestra la animacion de la UI y esconde el formulario de registro:
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            SigninFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            SigninFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    SigninFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            ProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            ProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    ProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            ProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            SigninFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /*
     * Metodo para generar Toasts a partir de unos parametros de entrada
     */
    private void makeToast(String text, int option){
        Toast.makeText(getApplicationContext(),text,option).show();
    }

    /**
     * Esta clase representa el proceso de sign in en la aplicacion, contiene la logica de registro.
     * El proceso se realiza mediante una AsyncTask que se ejecuta por debajo del Task
     * que la ha llamdo y no modifica la vista en ningun momento.
     */
    public class UserSigninTask extends AsyncTask<Void, Void, Boolean> {
        private final User user; //Objeto User que contiene la informacion del intento de registro.

        public UserSigninTask() {
            user = myUser;
        }

        /* Este metodo se llamara automaticamente al hacer un execute del AsyncTask. Contiene la
         * logica de registro. El metodo realiza una query para comprobar si hay coincidencia de email
         * si no la hay comprueba que no haya coincidencia de DNI y en caso de no haberla,
         * devuleve true. Si no hay conexion o el email o DNI estan registrados
         * se informa a el usuario y el metodo devolvera falso.*/
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected Boolean doInBackground(Void... params) {
            ParseQuery<ParseObject> query = ParseQuery.getQuery("User");
            query.whereEqualTo(User.EMAIL, user.getEmail());
            try {
                if(query.count() != 1) { //Block the calling thread, Si hay no coincidencia de email
                    ParseQuery<ParseObject> query2 = ParseQuery.getQuery("User");
                    query2.whereEqualTo(User.DNI, user.getDni());
                    if(query2.count() != 1){ //Block the calling thread, Si hay no coincidencia de DNI
                        user.saveInBackground(); //Guardar el objeto
                        return true;
                    }
                    else{
                        dni.setError(getString(R.string.dni_ya_registrado));
                    }
                }
                else{
                    email.setError(getString(R.string.email_ya_registrado));
                }
                signInError.setText(getString(R.string.compruebe_campos));
            } catch (ParseException e) {
                signInError.setText(getString(R.string.error_conexion));
            }
            return false;
        }

        /**
         * Este metodo se ejecuta automaticamente despues de doInBackground, obtiene como parametro
         * el valor devuelto por el metodo anterior y dependiendo de este valor, registrara a el usuaurio
         * en la app (si ha devuelto true) o reportara un error al usuario (si ha devuelto false).
         * */
        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                makeToast(getString(R.string.registroCorrecto),0);
                finish();
            } else {
                signInError.setVisibility(TextView.VISIBLE);
                if(dni.getError() != null)
                    dni.requestFocus();
                else if(email.getError() != null)
                    email.requestFocus();
                else
                    makeToast(getString(R.string.errorConexionServ),0);
                dni.requestFocus();
                makeToast(getString(R.string.errorRegistro),0);
            }
        }
    }
}
