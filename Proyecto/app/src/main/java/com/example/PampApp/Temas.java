package com.example.PampApp;

/**
 * Interfaz de temas: contiene los Strings con los nombres de los temas de la aplicación que se
 * utilizarán en varios Activities.
 */

public interface Temas {
    public final static int THEME_DEFAULT = R.style.DefaulTheme;
    public final static int THEME_DEFAULT_NBAR = R.style.DefaulTheme_NoActionBar;
    public final static int THEME_DARK = R.style.DefaultDarkTheme;
    public final static int THEME_DARK_NBAR = R.style.DefaultDarkTheme_NoActionBar;
    public final static int THEME_BLUE_LIGHT = R.style.BlueLightTheme;
    public final static int THEME_BLUE_LIGHT_NBAR = R.style.BlueLightTheme_NoActionBar;
    public final static int THEME_BLUE_DARK = R.style.BlueDarkTheme;
    public final static int THEME_BLUE_DARK_NBAR = R.style.BlueDarkTheme_NoActionBar;
    public final static int THEME_RED_LIGHT = R.style.RedLightTheme;
    public final static int THEME_RED_LIGHT_NBAR = R.style.RedLightTheme_NoActionBar;
    public final static int THEME_RED_DARK = R.style.RedDarkTheme;
    public final static int THEME_RED_DARK_NBAR = R.style.RedDarkTheme_NoActionBar;
    public final static int THEME_DIALOG = R.style.AlertDialogCustom;

}