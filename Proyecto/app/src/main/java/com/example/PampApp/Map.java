package com.example.PampApp;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.PampApp.Modelos.Alert;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.modes.CameraMode;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Semaphore;

import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap;
import static com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage;

public class Map extends AppCompatActivity implements OnMapReadyCallback, PermissionsListener, MapboxMap.OnMapLongClickListener, MapboxMap.OnMapClickListener {

    //Constantes:
    private static final int SHOW_SUBACTIVITY = 1; //Campo para llamar a la actividad de crear alerta.
    private final int ACTIVAR_UBICACION = 0; //Codigo de la actividad que nos servira para saber si el usuario a activado o no la ubicacion
    private final String DESTINATION_SOURCE = "destination-source-id"; //Vairiable que almacena la fuente para el layer que aparece al hacer click en el mapa
    private final String DESTINATION_LAYER = "destination-symbol-layer-id"; //Variable que almacena el nombre del layer que aparece al hacer click en el mapa.
    private final String CLUSTER_SOURCE = "cluster-source-id"; //Vairiable que almacena la fuente para el layer cluster.
    private final String CLUSTER_LAYER = "destination-cluster-layer-id"; //Variable que almacena el nombre del cluster layer.
    public static final int MAX_DISTANCIA = 3; //Distancia maxima en kilometros para compobar las alertas cercanas
    private final int TIEMPO_REFRESCO = 15000; //Tiempo de refresco del hijo en milisegundos
    private final double ZOOM_FIND = 15.0; //Zoom al pulsar el boton de localizacion
    private final double DISTANCIA_MINIMA = 0.1; //Distancia minima para considerar un punto.
    private final int DESCRIPCION = 30; //Longitud de la descripcion.

    //Componentes de xml
    private MapView mapView;
    private ImageButton locationButton; //Boton de localizacion
    private ImageButton styleButton; //Boton para cambiar de estilo

    //Card que se mostrara cuando un usuario coloque un marker en el mapa.
    private TextView direccion; //Campo que muestra la direccion seleccionada en el mapa
    private CardView mapCard; //Campo que contiene el texto y los botones que se muestran al seleccionar un punto del mapa
    private Button crearAlerta; //Boton para crear alerta desde el mapa.
    private Button cerrarCardMapa; //Boton para cerrarCardMapa el mapCard.

    //Card que se mostrara cuando un usuario haga click en una alerta del mapa.
    private TextView descripcion; //Campo que muestra una pequeña descripcion de la alerta seleccionada.
    private CardView alertCard; //Campo que contiene el texto y los botones que se muestran al seleccionar un punto del mapa
    private Button verAlerta; //Boton para ver alerta desde el mapa.
    private Button cerrarCardAlerta; //Boton para cerrarCardMapa el alertCard.
    private ImageView imagenAlerta; //ImageView con la imagen de la alerta.

    //Componentes funcionalidades:
    private PermissionsManager permissionsManager;
    private MapboxMap mapboxMap;
    private boolean style; //Varible que indica si el style es el basico (false) o satelite (true)
    private LocationComponent locationComponent; //Variable para obtener la localizacion actual
    private Style.Builder basicStyle; //Variable que almacena el estilo basico del mapa
    private String satelliteStyle; //Variable que almacena el estilo satelite del mapa
    private Style.OnStyleLoaded loadedStyle; //Variable que almacenara el los añadidos del mapa
    private Point destinationPoint; //Variable que alamacena el ultimo punjto seleccionado en el mapa
    private boolean stop;
    private boolean pause;
    private Thread hijo;
    private boolean markerSelected = false;
    private ArrayList<ParseObject> alertas;
    private Semaphore accesoLista; //Semaforo para controlar el acceso a la lista
    private ParseObject  alertaSeleccionada; //Alerta seleccionada al hacer click en el mapa.
    private BroadcastReceiver mGpsSwitchStateReceiver; //BoradcastReciver para saber cuando un usuario activa o desactiva gps
    private AlertDialog alertDialog; //Guardar una variable para el alertDialog que permitira cerrarlo cuando deba crearse uno nuevo

    private String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Ajustes.onActivityCreateSetAjustes(this,0);
        pause = false;
        stop = false;

        user=getIntent().getStringExtra("user");
        //Obtener una instancia de mapbox a traves de la clave de usuario:
        Mapbox.getInstance(this, getString(R.string.map_token));

        //Cargar el xml que contiene el mapa
        setContentView(R.layout.map);

        //Inicializar estilos
        satelliteStyle = Style.SATELLITE_STREETS;
        basicStyle = new Style.Builder().fromUrl("mapbox://styles/julenmerchan/cjt5wj3nr066u1fpa3k23icyc");
        style = false; //Colocar variable estilo a falso (primer estilo es basico)

        //Inicializar los elementos del XML:
        mapView = findViewById(R.id.mapView);

        //Inicializar el mapCard:
        mapCard = findViewById(R.id.MapCard);
        mapCard.bringToFront();
        direccion = findViewById(R.id.Direccion);

        //Inicializar el alertCard:
        alertCard = findViewById(R.id.AlertCard);
        alertCard.bringToFront();
        descripcion = findViewById(R.id.Descripcion);

        locationButton = findViewById(R.id.location_button);
        locationButton.bringToFront();
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findMe();
            }
        });

        styleButton = findViewById(R.id.styleButton);
        styleButton.bringToFront();
        styleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeStyle();
            }
        });

        crearAlerta = findViewById(R.id.CreaAlertaMapa);

        //Inicializar los botones del mapCard:
        crearAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Crear alerta:
                Intent intent= new Intent(getApplicationContext(),nuevaAlerta.class);
                Bundle bundle = new Bundle(); //Crear bundle para enviar coordenadas
                bundle.putDouble(String.valueOf(R.string.latitud_alerta),destinationPoint.latitude()); //Guardar latitud
                bundle.putDouble(String.valueOf(R.string.longitud_alerta),destinationPoint.longitude()); //Guardar longitud
                bundle.putString(String.valueOf(R.string.direccion_alerta),direccion.getText().toString()); //Guardar direccion.
                intent.putExtras(bundle);
                intent.putExtra("user",user);
                startActivityForResult(intent,SHOW_SUBACTIVITY); //Iniciar la Subactividad
            }
        });

        cerrarCardMapa = findViewById(R.id.CerrarCardMapa);
        cerrarCardMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Cerrar mapCard:
                eliminaLayer();
                mapCard.setVisibility(View.INVISIBLE);
            }
        });

        imagenAlerta = findViewById(R.id.imagenAlerta);
        //Inicializar los botones del alertCard:
        verAlerta = findViewById(R.id.verAlertaMapa);

        //Inicializar los botones del mapCard:
        verAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Ver alerta:
                if(alertaSeleccionada != null) {
                    Intent intent = new Intent(getApplicationContext(), verAlerta.class);
                    Bundle bundle = new Bundle(); //Crear bundle para enviar el id de la alerta
                    bundle.putString("id", alertaSeleccionada.getObjectId()); //Guardar id alerta.
                    intent.putExtras(bundle); //Guardar el bundle
                    intent.putExtra("user",user);
                    startActivityForResult(intent, SHOW_SUBACTIVITY); //Iniciar la Subactividad
                }
            }
        });

        cerrarCardAlerta = findViewById(R.id.CerrarCardAlerta);
        cerrarCardAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Cerrar AlertMap:
                deselectMarker((SymbolLayer) mapboxMap.getStyle().getLayer(DESTINATION_LAYER));
                deselectAlertCard();
            }
        });
        mapView.onCreate(savedInstanceState);
        alertas = new ArrayList<>();
        accesoLista = new Semaphore(1);

        registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)); //Registrar el broadcastReciever (tambien se pede hacer desde el manifest pero para ello deberiamos crear una clase que extienda a BroadcastReciver)

        //Crear un BroadCastReciever para saber cuando un usuario activa o desactiva los servicios de ubicacion:
         mGpsSwitchStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")){ //Si han cambiado los proveedores de servicio o de locaizacion
                    //Comprobar los servicios de red y ubicacion:
                    if(!checkLocationServices()) { //Si se han desactivado pausar al hijo
                        pause = true;
                        Toast.makeText(context,getString(R.string.serviciosDesactivados),Toast.LENGTH_LONG).show();
                    }
                    else{ //Si se han activado
                        findMe(); //Mostrar localizacion.
                        if(hijo!=null) {//Si el hijo estaba dormido, reactivarlo:
                            if (pause)
                                despierta();
                        }
                        else {//Si el hijo no estaba creado aun:
                            //Comenzar a printear los puntos en el mapa
                            printCluster();
                        }
                    }
                }
            }
        };
        mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NonNull final MapboxMap mapboxMap) {
        Map.this.mapboxMap = mapboxMap;

        //Iinicializar la variable que almacenara los añadidos del mapa
        loadedStyle = new Style.OnStyleLoaded() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onStyleLoaded(@NonNull Style style) {
                addClusterSymbolLayer(style);
                addDestinationIconSymbolLayer(style);


                mapboxMap.addOnMapLongClickListener(Map.this);
                mapboxMap.addOnMapClickListener(Map.this);
                 enableLocationComponent(style);
            }
        };
        mapboxMap.setStyle(basicStyle, loadedStyle);
    }

    /* Metodo que se encarga de activar el elemento de localizacion, para ello pide permisos al uuario
     * (en caso de no haberlos pedido antes) y si el usuario acepta podra utilizar el mapa en caso
      * contrario se terminara el activity.*/
    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressWarnings( {"MissingPermission"})
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        //Comprobar si se han concedido los permisos de ubicacion:
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
                //Obtener una instancia del componente de localizacion
                locationComponent = mapboxMap.getLocationComponent();

                //Activar el componente
                locationComponent.activateLocationComponent(this, loadedMapStyle);

                if(checkLocationServices()){//Comprobar si estan activados los servicios de red y ubicacion.
                    //Hacer el indicador visible
                    locationComponent.setLocationComponentEnabled(true);

                    //Establecer el compas (brujula)
                    locationComponent.setRenderMode(RenderMode.COMPASS);

                    //Comenzar a printear los puntos en el mapa
                    printCluster();

                }
        }
        else if(shouldShowRequestPermissionRationale(LOCATION_SERVICE)){ //Si no se han concedido permisos y el usuario no ha marcado la opcion no volver a preguntar, solicitarlos
                permissionsManager = new PermissionsManager(this);
                permissionsManager.requestLocationPermissions(this);
        }
        else{ //Si el usuario marcó la opción no volver a solicitar permisos: (Requiere API 23 o mayor):
            finish();
            Toast.makeText(this,getString(R.string.activarPermisos),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {
        Toast.makeText(this, getString(R.string.user_location_permission_explanation), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) { //Si se han dado permisos habilitar la componente de localizacion
            mapboxMap.getStyle(new Style.OnStyleLoaded() {

                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onStyleLoaded(@NonNull Style style) {
                    enableLocationComponent(style);
                }
            });
        } else {
            Toast.makeText(this, getString(R.string.user_location_permission_not_granted), Toast.LENGTH_LONG).show();
            finish();
        }
    }

    /*Metodo asociado a el boton de localizar, permite posicionar el mapa en la ubicacion actual del usuario*/
    public void findMe(){
        //Comprobar que los servicios de ubicacion estan activados:
        if(checkLocationServices()) {
            try {
                //Hacer el indicador visible
                locationComponent.setLocationComponentEnabled(true);

                //Colocar el modo de la camara en Tracking
                locationComponent.setCameraMode(CameraMode.TRACKING);

                //Establecer el compas (brujula)
                locationComponent.setRenderMode(RenderMode.COMPASS);

                //Hacer zoom a la ubicacion del usuario
                locationComponent.zoomWhileTracking(ZOOM_FIND);
            }catch(SecurityException e){
                Log.v("Última localización","No se pudo obtener la última localizacion del usuario");
            }
        }
    }

    /* Metodo que permite cambiar el estilo del mapa, es decir, permite pasar de modo satelite al modo basico
        y viceversa.
     */
    public void changeStyle() {
        //Comprobar primero si se estaba mostrando una direccion y ocultarla
        if(mapCard.getVisibility() == View.VISIBLE)
            mapCard.setVisibility(View.INVISIBLE);
        //Comprobar si el usuario tenia una alerta seleccionada y deseleccionarla
        if(alertCard.getVisibility() == View.VISIBLE)
            deselectAlertCard();
        if(!style){
            mapboxMap.setStyle(satelliteStyle,loadedStyle);
            style = true;
        }
        else{
            mapboxMap.setStyle(basicStyle,loadedStyle);
            style = false;
        }
    }

    /* Metodo que permite chequear los servicios de acceso para ver si estan activados o no. Devuelve
    *  falso si estan desactivados y verdadero en caso contrario.*/
    public boolean checkLocationServices(){
        LocationManager lm = (LocationManager)this.getSystemService(LOCATION_SERVICE);
        boolean gps = false;
        boolean network = false;

        try {
            gps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}
        if(alertDialog != null)
            alertDialog.dismiss();
        if(!gps){
                //Notificar al usuario que tiene desactivados los servicios de ubicacion.
                alertDialog = new AlertDialog.Builder(this,Temas.THEME_DIALOG)
                        .setMessage(R.string.gps_desactivado)
                        .setPositiveButton(R.string.activar_gps, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),ACTIVAR_UBICACION);
                            }
                        }).setNegativeButton(R.string.cancelar_gps, null)
                        .show();
                return false;
        }
        else if(!network){
            //Notificar al usuario que tiene desactivados los servicios de red.
            alertDialog = new AlertDialog.Builder(this,Temas.THEME_DIALOG)
                    .setMessage(R.string.net_desactivada)
                    .setPositiveButton(R.string.activar_gps, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),ACTIVAR_UBICACION);
                        }
                    }).setNegativeButton(R.string.cancelar_gps, null)
                    .show();
            return false;
        }
        return true;
    }

    /* Metodo para chequear los resultados de las subactiviades:*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTIVAR_UBICACION: //Comprobar si el usuario ha activado o no los activity_ajustes de ubicacion.
                findMe();
                break;
        }
    }

    /*Metodo que se encarga de añadir el layer del marker al mapa cuando se le añade un estilo al mismo. */
    private void addDestinationIconSymbolLayer(@NonNull Style loadedMapStyle) {
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource(DESTINATION_SOURCE);
        if(loadedMapStyle.getSource(DESTINATION_SOURCE) == null) { //comprobar que no esta cargado el estilo por si acaso hay un cambio rapido de estilos
            loadedMapStyle.addSource(geoJsonSource);
            addLayer(DESTINATION_LAYER, DESTINATION_SOURCE, loadedMapStyle);
        }
    }

    private void addClusterSymbolLayer(@ NonNull Style loadedMapStyle){
        loadedMapStyle.addImage("destination-icon-id",
                BitmapFactory.decodeResource(this.getResources(), R.drawable.mapbox_marker_icon_default));
        GeoJsonSource geoJsonSource = new GeoJsonSource(CLUSTER_SOURCE);
        if(loadedMapStyle.getSource(CLUSTER_SOURCE) == null) { //comprobar que no esta cargado el estilo por si acaso hay un cambio rapido de estilos
            loadedMapStyle.addSource(geoJsonSource);
            addLayer(CLUSTER_LAYER, CLUSTER_SOURCE, loadedMapStyle);
        }
    }

    /* Metodo que se encarga de añadir el layer a el mapa: */
    private void addLayer(String layer, String source, Style loadedMapStyle){
        SymbolLayer symbolLayer = new SymbolLayer(layer, source);
        symbolLayer.withProperties(
                iconImage("destination-icon-id"),
                iconAllowOverlap(true)
        );
        loadedMapStyle.addLayer(symbolLayer);
    }

    /* Este metodo se encarga de crear un hilo que cada x segundos realiza una query al servidor Parse
    * para obtener las alertas cercanas a la ubicacion del usuario y poder printearlas en el mapa*/
    public void printCluster(){
        Toast.makeText(getApplicationContext(),getString(R.string.obteniendoUbicacion),Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(),getString(R.string.obteniendoAlertas)+"("+MAX_DISTANCIA+" km)",Toast.LENGTH_LONG).show();
        Runnable hilo = new Runnable() {
            @SuppressLint("MissingPermission")
            public void run() {
                Location myLocation;
                do{
                    try{
                        Thread.sleep(TIEMPO_REFRESCO); //Esperar un tiempo.
                        if(pause){ //Si la actividad es pausada o pasa a segundo plano debemos pausar el hilo
                            synchronized (hijo){
                                    hijo.wait();
                            }
                        }
                        if(locationComponent != null) { //Comprobar que la componente de localizacion esta activada
                            myLocation = locationComponent.getLastKnownLocation(); //Obtener la ultima ubicacion del usuario
                            /*Si la ubicacion no es nula, hacer una query que obtenga todas las alertas a una distancia menor o igual
                             * aMAX_DISTANCIA y mostrarlas en el mapa. */
                            if(myLocation != null) {
                                ParseGeoPoint userLocation = new ParseGeoPoint(myLocation.getLatitude(),myLocation.getLongitude());
                                ParseQuery<ParseObject> query = ParseQuery.getQuery("Alert");
                                query.whereWithinKilometers("localizacion", userLocation, MAX_DISTANCIA);
                                query.setLimit(50);
                                query.findInBackground(new FindCallback<ParseObject>() {
                                    @Override
                                    public void done(List<ParseObject> queryresult, ParseException e) {
                                        if(e == null) {
                                            if (!queryresult.isEmpty()) {
                                                if (mapboxMap.getStyle() != null){ //Comprobar que el estilo no sea nulo por si hay un cambio rapido de estilo
                                                    GeoJsonSource source = mapboxMap.getStyle().getSourceAs(CLUSTER_SOURCE);
                                                    List<Feature> lista = new ArrayList<>();
                                                    guardarAlertas(queryresult); //Guardar el resultado de la query

                                                    //Crear la lista de Feature a partir de los objetos devueltos en la lista, despues de realizar la query
                                                    for (ParseObject alerta : queryresult) {
                                                        ParseGeoPoint loc = ((Alert) alerta).getLocalizacion();
                                                        lista.add(Feature.fromGeometry(Point.fromLngLat(loc.getLongitude(), loc.getLatitude())));
                                                    }
                                                    //Apartir de lista de Feature construir una FeatureCollection y añadirla a la fuente para mostrarla
                                                    FeatureCollection feature = FeatureCollection.fromFeatures(lista);
                                                    if (source != null) {
                                                        source.setGeoJson(feature);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    } catch (InterruptedException e) {
                        Log.v("Hijo interrumpido","El hijo ha sido interrumpido");
                    }
                }while (!stop);
            }
        };
       hijo = new Thread(hilo);
       hijo.start();
    }

    /* Metodo que almacena las alertas que recoge el hilo hijo. requiere de acceso al semaforo por si el padre esta leyendo
     * la lista. */
    private void guardarAlertas(List<ParseObject> queryresult){
        try {
            accesoLista.acquire();
            alertas.clear();
            alertas.addAll(queryresult);
            accesoLista.release();
        } catch (InterruptedException e) {
            Log.v("Hijo interrumpido","El hijo ha sido interrumpido cuando accedia a lista");
        }
    }


    /* Metodo que coloca en el mapa el marcador cuando el usuario hace click en un punto del mismo
     * durante un tiempo determinado. Ademas del marcador aparece un cuadro de texto con la direccion
      * de la calle asi como como un par de botones para poder cerrarCardMapa el cuadro o crear una alerta.*/
    @SuppressWarnings( {"MissingPermission"})
    @Override
    public boolean onMapLongClick(@NonNull LatLng point) {
        //Comprobar si hay un marker seleccionado y en ese caso deseleccionarlo y cambiar el tamaño del marker a su estado habitual.
        if(markerSelected){
            mapboxMap.getStyle().getLayer(DESTINATION_LAYER).setProperties(
                    PropertyFactory.iconSize(1.0f)
            );
            deselectAlertCard();
        }

        destinationPoint = Point.fromLngLat(point.getLongitude(), point.getLatitude());

        GeoJsonSource source = mapboxMap.getStyle().getSourceAs(DESTINATION_SOURCE);
        if (source != null) {
            source.setGeoJson(Feature.fromGeometry(destinationPoint));
        }
        //Utilizar geocoder para obtener la direccion del marcador:
        Geocoder geocoder =  new Geocoder(this);
        List<Address> address = null;

        try { //Obtener la direccion, printearla en el cuadro de texto y mostrarla
           address = geocoder.getFromLocation(point.getLatitude(),point.getLongitude(),1);
           if(!address.isEmpty()) {
               direccion.setText(address.get(0).getAddressLine(0));
               mapCard.setVisibility(View.VISIBLE);
               return true;
           }
        } catch (IOException e) { //En caso de que el geocoder no haya podido encontrar la direccion:
            Toast.makeText(this,getString(R.string.errorObtenerDireccion),Toast.LENGTH_SHORT).show();
        }
        //Si el Geocoder no ha encontrado la direccion imprimira las coordenadas en el cuadro de texto.
        direccion.setText("(" +  point.getLatitude() + ", " + point.getLongitude() + ")");
        mapCard.setVisibility(View.VISIBLE);
        return true;
    }

    /* Cuando un usuario pulsa en una zona del mapa este metodo que comprueba si hay una marcador
    * en el mismo y si el mapCard esta visible en caso de ser asi elimina el marcador y coulta el mapCard.*/
    @Override
    public boolean onMapClick(@NonNull LatLng point) {
        //Primero comprobar si el mapCard está visible por si hay un marcador colocado en el mapa:
        if(mapCard.getVisibility() == View.VISIBLE){
            mapCard.setVisibility(View.INVISIBLE);
            eliminaLayer();
        }
        Style style = mapboxMap.getStyle();
        if (style != null) {
            final SymbolLayer selectedMarkerSymbolLayer = (SymbolLayer) style.getLayer(DESTINATION_LAYER);

            //Recoger la pulsacion en pantalla del usuario
            final PointF pixel = mapboxMap.getProjection().toScreenLocation(point);

            //Para el pixel donde ha pulsado el usuario recoger todas las features que hay y coger la seleccionada:
            List<Feature> features = mapboxMap.queryRenderedFeatures(pixel, CLUSTER_LAYER); //Para las features las cogemos del cluster
            List<Feature> selectedFeature = mapboxMap.queryRenderedFeatures(pixel, DESTINATION_LAYER); //La feature seleccionada la cogemos del destination, evitando asi tener dos features en un mismo pixel y no deje deseleccionar.

            //Si el tamaño de la feature seleccionada es mayor que 0 y ya hay un marker seleccionado no hacer nada
            if (selectedFeature.size() > 0 && markerSelected) {
                return false;
            }

            /*Si el pixel en el que ha pulsado no tiene features, comprobar si hay un marker seleccionado y deseleccionar, comprobar
             ademas si el alertCard esta visible y en caso afirmativo ocultarlo.
             */
            if (features.isEmpty()) {
                if (markerSelected) {
                    deselectMarker(selectedMarkerSymbolLayer);
                    if(alertCard.getVisibility() == View.VISIBLE) {
                        deselectAlertCard();
                    }
                }
                return false;
            }

            //Si el pixel en el que ha pulsado tiene features y ademas no es una feature seleccionada, deseleccionar la anterior y seleccionar esta:
            GeoJsonSource source = style.getSourceAs(DESTINATION_SOURCE);
            if (source != null) {
                source.setGeoJson(FeatureCollection.fromFeatures(
                        new Feature[]{Feature.fromGeometry(features.get(0).geometry())}));
            }
            //Si hay algun Marker seleccionado deseleccionarlo.
            if (markerSelected) {
                deselectMarker(selectedMarkerSymbolLayer);
            }
            //Seleccionar el nuevo marker y mostrar el alertCard con la info de la alerta.
            if (features.size() > 0) {
                alertaSeleccionada = buscaPunto(point);
                if(alertaSeleccionada != null) { //En caso de encontrar alerta, mostrarla
                    selectMarker(selectedMarkerSymbolLayer);
                    descripcion.setText(getString(R.string.tituloAlertaMapa)+" "+alertaSeleccionada.get("titulo")+"\n"+" "+getString(R.string.descripcionAlertaMapa)+" "+((String)alertaSeleccionada.get("descripcion")).substring(0,DESCRIPCION)+"...");
                    byte[] foto = (byte[]) alertaSeleccionada.get("foto");
                    imagenAlerta.setImageBitmap(BitmapFactory.decodeByteArray(foto,0,foto.length));
                    alertCard.setVisibility(View.VISIBLE);
                }
            }
        }
        return true;
    }

    /** Este método determina si cuando un usuario pulsa en el mapa ha pulsado o no un marker existente.
     * Contiene la lógica principal de ver una alerta desde el mapa. Cuando un usuario pulsa en el mapa,
     * en funcíon del zoom de la cámara se ajusta el valor de una cierta tolerancia, todas aquellas alertas
     * cuya distancia euclidea a donde ha pulsado el usuario sea menor o igual que la tolerancia ajustada, son
     * posibles alertas pulsadas, de entre todas ellas se elige a la que menor distancia esté (tenga menor tolerancia)**/
    private ParseObject buscaPunto(LatLng miPunto){
        ParseObject result = null;
        try {
            if(alertas.isEmpty())
                return result; //Comprobar que la lista no sea vacia antes de coger ningun permiso
            accesoLista.acquire(); //Coger el permiso para evitar que el hijo pueda borrar la lista mientras es comprobadas
            double distancia;
            CameraPosition camera = mapboxMap.getCameraPosition(); //Obtener la camara actual para obtener el zoom.
            double dist_min = DISTANCIA_MINIMA / camera.zoom; //Obtener la distancia minima en funcion del zoom actual (tolerancia)
            double minimo = dist_min + 1;
            Iterator<ParseObject> iterator = alertas.iterator();

            //Comprobar que alertas cumplen que su distancia sea menor o igual a la tolerancia y de estas coger la menor.
            while(iterator.hasNext()){
                ParseObject alerta = iterator.next();
                ParseGeoPoint puntoAlerta = (ParseGeoPoint) alerta.get("localizacion");
                //Calcular la distancia euclidea entre el punto seleccionado por el usuario y la alerta
                distancia = Math.sqrt(Math.pow(puntoAlerta.getLatitude()-miPunto.getLatitude(),2)+Math.pow(puntoAlerta.getLongitude()-miPunto.getLongitude(),2));
                if(distancia <= dist_min){
                    if(distancia < minimo) {
                        minimo = distancia;
                        result = alerta;
                    }
                }
            }
        } catch (InterruptedException e) {
            Log.v("Padre interrumpido","El padre ha sido interrumpido cuando accedia a la lista");
        }
        accesoLista.release();
        if(result !=null) {
            //Actualizar vista de la camara y centrarla en el punto seleccionado:
            CameraPosition position = new CameraPosition.Builder()
                    .target(new LatLng(miPunto.getLatitude(), miPunto.getLongitude()))
                    .build();
            mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
        }
        return result;
    }

    /** Metodo que realiza la animación de seleccionar un marker del mapa */
    private void selectMarker(final SymbolLayer iconLayer) {
        ValueAnimator markerAnimator = new ValueAnimator();
        markerAnimator.setObjectValues(1f, 2f);
        markerAnimator.setDuration(300);
        markerAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                iconLayer.setProperties(
                        PropertyFactory.iconSize((float) animator.getAnimatedValue())
                );
            }
        });
        markerAnimator.start();
        markerSelected = true;
    }

    /** Metodo que realiza la animación de deselccionar un marker del mapa */
    private void deselectMarker(final SymbolLayer iconLayer) {
        ValueAnimator markerAnimator = new ValueAnimator();
        markerAnimator.setObjectValues(2f, 1f);
        markerAnimator.setDuration(300);
        markerAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                iconLayer.setProperties(
                        PropertyFactory.iconSize((float) animator.getAnimatedValue())
                );
            }
        });
        markerAnimator.start();
        markerSelected = false;
    }

    /** Metodo permite ocultar el cuadro de alerta seleccionada **/
    private void deselectAlertCard(){
        alertCard.setVisibility(View.INVISIBLE); //Ocultar el card
        imagenAlerta.setImageDrawable(null); //Eliminar la imagen
        alertaSeleccionada = null; //Establecer que no hay una alerta selccionada
    }

    /* Este metodo se encarga de eliminar el layer actual del mapa (Marker). */
    public void eliminaLayer(){
        mapboxMap.getStyle().removeLayer(DESTINATION_LAYER);//Elimina el Layer
        mapboxMap.getStyle().removeSource(DESTINATION_SOURCE); //Elimina la fuente
        addDestinationIconSymbolLayer(mapboxMap.getStyle()); //Añade la fuente
    }

    /* Metodo que se encarga de despertar al hijo*/
    public void despierta(){
        pause = false;
        if(hijo != null) {
            synchronized (hijo) {
                hijo.notifyAll(); //Notificar al hijo que puede continuar su ejecucion
            }
        }
    }

    @Override
    @SuppressWarnings( {"MissingPermission"})
    protected void onStart() {
        try {
            super.onStart();
            mapView.onStart();
        }catch(SecurityException ex){
            Log.v("Última localización","No se pudo obtener la última localizacion del usuario");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
        despierta();
        registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION)); //Registrar el broadcastReciever (tambien se pede hacer desde el manifest pero para ello deberiamos crear una clase que extienda a BroadcastReciver)
    }

    @Override
    protected  void onRestart() {
        super.onRestart();
        despierta();
    }

    @Override
    protected void onPause() {
        pause = true; //Pausar el hilo hijo
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        pause = true; //Pausar el hilo hijo
        super.onStop();
        mapView.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        stop = true;
        if(hijo != null) {
            if(hijo.getState() == Thread.State.TIMED_WAITING) //Si el hijo está durmiendo:
                hijo.interrupt(); //Interrumpir el hilo
        }
        unregisterReceiver(mGpsSwitchStateReceiver); //Eliminar el broadcastReciever
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
